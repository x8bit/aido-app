'use strict';
angular.module('main')
.directive('disableSideMenuDrag', function ($ionicSideMenuDelegate, $rootScope) {

  return {
    restrict: 'A',
    controller: function ($scope, $element) {

      function stopDrag () {
        $ionicSideMenuDelegate.canDragContent(false);
      }

      function allowDrag () {
        $ionicSideMenuDelegate.canDragContent(true);
      }

      $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
      $element.on('touchstart', stopDrag);
      $element.on('touchend', allowDrag);
      $element.on('mousedown', stopDrag);
      $element.on('mouseup', allowDrag);

    }
  };

});
