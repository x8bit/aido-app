'use strict';
angular.module('main')
.directive('firebaseImage', function ($rootScope, $state, ImageLoader) {

  return {
    replace: false,
    link: function ($scope, $element, $attr) {
      var fullscreen = ($attr.fullscreen) ? true : false;
      var storage = window.firebase.storage();
      var firebaseRef = $attr.firebaseRef;
      ImageLoader.setImage(firebaseRef);
      storage.refFromURL(firebaseRef).getDownloadURL().then(function (url) {
        var img = $element.find('img');
        if (fullscreen) {
          img.addClass('fullscreen-image');
        }
        img.attr('src', url);
        img.on('load', function () {
          img.removeAttr('style');
          ImageLoader.loaded(firebaseRef);
          $rootScope.$broadcast('image-loaded', {queue: ImageLoader.imagesToLoad.length});
        });
      });
    },
    template: '<img src="main/assets/images/no-image-placeholder-big.png" style="display: block; width: 100%;height: 100%;">'
  };

});
