'use strict';
angular.module('main')
.directive('sideMenu', function ()  {
  return {
    restrict: 'A',
    link: function (scope, element) {
      scope.margin = element[0].clientHeight - (174) - 154;
      document.getElementById('second-list').style['margin-top'] = scope.margin + 'px';
    }
  };
});
