'use strict';
angular.module('main')
.directive('focusNext', function ()  {
  return {
    restrict: 'A',
    link: function ($scope, element) {
      element.bind('keydown', function (e) {
        var code = e.keyCode || e.which;
        if (code === 13) {
          e.preventDefault();
          //element.next()[0].focus(); This can be used if don't have  input fields nested inside other container elements
          var pageElements = document.querySelectorAll('input, select, textarea');
          var focusNext = false,
            len = pageElements.length;
          for (var i = 0; i < len; i++) {
            var elem = pageElements[i];
            if (focusNext) {
              if (elem.style.display !== 'none') {
                elem.focus();
                break;
              }
            } else if (elem === e.srcElement) {
              focusNext = true;
            }
          }
        }
      });
    }
  };
});
