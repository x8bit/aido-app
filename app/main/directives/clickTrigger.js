'use strict';
angular.module('main')
.directive('clickTrigger', function ($rootScope)  {
  return {
    restrict: 'A',
    link: function (scope, el) {
      var handler = function (evt) {
        $rootScope.$broadcast('click-triggered', { event: evt });
      };
      el.off('click', handler);
      el.on('click', handler);
      el.triggerHandler('click');
    }
  };
});
