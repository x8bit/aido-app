'use strict';
angular.module('main')
.directive('activePageHighlight', function ($rootScope, $state) {
  return {

    link: function ($scope, $element, $attr) {

      function checkUISref () {
        if ($state.is($attr['uiSref'])) {
          $element.addClass('active-page-highlight');
        } else {
          $element.removeClass('active-page-highlight');
        }
      }

      checkUISref();

      $rootScope.$on('$stateChangeSuccess', function () {
        checkUISref();
      });
    }
  };
});
