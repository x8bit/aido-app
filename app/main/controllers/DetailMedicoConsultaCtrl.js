'use strict';
angular.module('main')
.controller('DetailMedicoConsultaCtrl', function ($scope, $stateParams, aidoAPIFactory, $rootScope, $state, AuthService, $ionicHistory) {
  var medicId = $stateParams.medic;
  var activeUserId = AuthService.sessionInfo().session.id;
  $scope.caseChat = $stateParams.case;
  $scope.medic = {};
  aidoAPIFactory.getUser(medicId).then(function (data) {
    $scope.medic = data.data;
  });

  $ionicHistory.nextViewOptions({
    disableBack: false
  });

  $scope.createChat = function () {
    $state.go('main.payments', {
      userId: activeUserId,
      medic: $scope.medic,
      case: $scope.caseChat
    });
    // var chatJSON = {};
    // chatJSON.participants = [parseInt(medicId), parseInt(activeUserId)];
    // chatJSON.caseChat = parseInt($scope.caseChat);
    // aidoAPIFactory.createChatRoom(chatJSON, $rootScope.csrfToken).then(function (response) {
    //   var chatId = response.data.id;
    //   $state.go('main.chat', {id: chatId});
    // });
  };

});
