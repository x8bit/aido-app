'use strict';
angular.module('main')
.controller('ChatCtrl', function ($scope, $stateParams, aidoAPIFactory, $ionicLoading, AuthService, $rootScope, $ionicScrollDelegate, $firebaseObject, $firebaseArray, CameraService, AttachmentsService, $ionicPopover, $state, $q, $ionicModal, Utils, $ionicPopup, $timeout, $ionicHistory) {
  $scope.user = AuthService.sessionInfo().session;
  $scope.roleIsPatient = AuthService.isCurrentUserPatient();

  var chat = $stateParams.id;
  var ref = window.firebase.database().ref('chat-messages/' + chat);
  var query = ref.limitToLast(25);
  var firstLoad = false;
  $scope.caso = {};

  $scope.isChatActive = false;

  $scope.chat = {};
  $scope.chat.title = 'Chat';
  $scope.chatInfo = {};
  $scope.otherUser = {};
  $scope.age = -1;

  ref.on('child_added', function () {
    if (firstLoad) {
      $scope.gotoBottom();
      console.log('-');
    }
  });

  $scope.notes = [];
  $scope.tabIsChat = true;

  $ionicPopover.fromTemplateUrl('main/templates/chatOptionsPopOver.html', {
    scope: $scope
  }).then(function (popover) {
    $scope.popover = popover;
  });

  $scope.imageSrc = '';

  $ionicPopover.fromTemplateUrl('main/templates/image-modal.html', {
    scope: $scope
  }).then(function (popover) {
    $scope.imageModal = popover;
  });

  $scope.openImageModal = function () {
    $scope.imageModal.show();
  };

  $scope.closeImageModal = function () {
    $scope.imageModal.hide();
  };

  $scope.showImage = function (src) {
    $scope.imageSrc = src.target.currentSrc;
    window.PhotoViewer.show($scope.imageSrc);
  };

  $scope.goToChat = function (chatId) {
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $state.go('main.chat', {caseId: $scope.chatInfo.caseChat.id, id: chatId});
  };

  // $scope.goToPrescription =  function (chat) {
  //   $state.go('main.consultarReceta', { medicId: chat.participants[0].id, caseTitle: $scope.chatInfo.caseChat.name , chatId: chat.id});
  // };

  //// User profile

  $ionicModal.fromTemplateUrl('main/templates/chatUserProfile.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function (modal) {
    $scope.userModal = modal;
  });

  $ionicModal.fromTemplateUrl('main/templates/consultaPerfil.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function (modal) {
    $scope.profileModal = modal;
  });

  $ionicModal.fromTemplateUrl('main/templates/detalleMedicoModal.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function (modal) {
    $scope.medicModal = modal;
  });

  $scope.openUserModal = function () {
    $scope.userModal.show();
  };

  $scope.openMedicModal = function () {
    $scope.medicModal.show();
  };

  $scope.closeUserModal = function () {
    $scope.userModal.hide();
  };

  $scope.closeModal = function () {
    $scope.imageModal.hide();
  };

  $scope.goToProfile = function () {
    $scope.profileModal.show();
  };

  $scope.goToHistory = function () {
    $scope.popover.hide();
    $state.go('main.casoHistory', { caseId: $scope.chatInfo.caseChat.id });
  };

  $scope.goToOptions = function ($event) {
    $scope.popover.show($event);
  };

  $scope.closeChat = function () {
    $scope.popover.hide();
    $ionicPopup.confirm({
      title: 'Aido',
      template: '¿Deseas cerrar la consulta?',
      cancelText: 'Cancelar',
      okText: 'Aceptar'
    }).then(function (res) {
      if (res) {
        aidoAPIFactory.closeChat($scope.chatInfo.id).then(function () {
          //$state.go('main.listaDeCasos');
          $scope.isChatActive = false;
          $scope.popoverHeight = '125px';
        });
      }
    });
  };

  $scope.openChat = function () {
    $scope.popover.hide();

    if ($scope.isCaseActive) {
      $ionicPopup.confirm({
        title: 'Aido',
        template: '¿Deseas reabrir la consulta?',
        cancelText: 'Cancelar',
        okText: 'Aceptar'
      }).then(function (res) {
        if (res) {
          aidoAPIFactory.openChat($scope.chatInfo.id).then(function () {
            //$state.go('main.listaDeCasos');
            $scope.isChatActive = true;
            $scope.popoverHeight = '125px';
          });
        }
      });
      // confirm('¿Deseas reabrir la consulta?', function (buttonIndex) {
      //   // no button = 0, 'OK' = 1, 'Cancel' = 2
      //   if (buttonIndex === 1) {
      //     aidoAPIFactory.openChat($scope.chatInfo.id).then(function () {
      //       //$state.go('main.listaDeCasos');
      //       $scope.isChatActive = true;
      //       $scope.popoverHeight = '125px';
      //     });
      //   }
      // }, 'Cerrar Caso', ['Aceptar', 'Cancelar']);
    } else {
      Utils.alert('El caso esta cerrado, no se puede reabrir la consulta', null, 'Aido', 'Aceptar');
    }
  };

  $scope.report = function () {
    $scope.popover.hide();
    $state.go('main.contacto');
  };

  $scope.goToMedicInfo = function () {
    if ($scope.roleIsPatient) {
      $scope.openMedicModal();
    } else {
      $scope.fillUser();
    }
  };

  $scope.fillUser = function () {
    aidoAPIFactory.getCaseDetail($scope.chatInfo.caseChat.id).then(function (data) {
      $scope.caso = data;

      var test = angular.copy($scope.caso.firebaseAttachments);
      $scope.arrOfArr = [];

      for (var i = 0; i < $scope.caso.firebaseAttachments.length; i++) {
        if ((i % 4 ) === 0) {
          $scope.arrOfArr.push(test.splice(0, 4));
        }
      }

      $scope.status = ($scope.caso.status === 'active') ? 'Caso Abierto' : 'Caso Cerrrado';
      // Quitar al paciente de los participantes del chat
      $scope.caso.chats.forEach(function (c) {
        c.participants = c.participants.filter(function (p) {
          return p.id !== $scope.caso.patient;
        });
      });
      // Si es médico solo mostrar sus consultas
      // if (!$scope.roleIsPatient) {
      //   $scope.caso.chats.forEach(function (c) {
      //     c.participants = c.participants.filter(function (p) {
      //       return p.id === $scope.user.id;
      //     });
      //   });
      // }
      $scope.openUserModal();
    });
  };

  $scope.goToPrescription = function () {
    $state.go('main.chat.receta', { chat: $scope.chatInfo.id, otherUser: $scope.otherUser.id });
  };

  $scope.goToPrescriptionNoEdit = function (chat) {
    $state.go('main.consultarReceta', {recetaId: chat.prescription[0].id, userId: $scope.otherUser.id });
  };

  $scope.$on('$destroy', function () {
    $scope.popover.remove();
    $scope.medicModal.remove();
    $scope.userModal.remove();
    $scope.imageModal.remove();
  });

  aidoAPIFactory.getChatRoom(chat).then(function (response) {
    $scope.chatInfo = response.data;
    if ($scope.chatInfo.caseChat.status === 'closed') {
      $scope.isCaseActive = false;
      $scope.popoverHeight = ($scope.roleIsPatient) ? '125px' : '170px';
    } else {
      $scope.isCaseActive = true;
      $scope.popoverHeight = ($scope.roleIsPatient) ? '125px' : '225px';
      $scope.isChatActive = ($scope.chatInfo.status === 'open') ? true : false;
    }
    $scope.chat.title = 'Chat: ' + $scope.chatInfo.caseChat.name;
    $scope.otherUser = $scope.chatInfo.participants.filter(function (p) {
      return p.id !== $scope.user.id;
    })[0];

    aidoAPIFactory.getUser($scope.otherUser.id).then(function (response) {
      $scope.otherUser = response.data;
      if (!$scope.roleIsPatient) {
        $scope.age = window.moment($scope.otherUser.profile.birthday).fromNow(true);
        $scope.age = $scope.age.replace(/\D/g, '');
      }
    });
  });

  aidoAPIFactory.getNotes(chat).then(function (response) {
    $scope.notes = response.data;
  });

  $scope.switchTabs = function (value) {
    $scope.tabIsChat = value;
    if ($scope.tabIsChat) {
      $scope.chat.title = 'Chat: ' + $scope.chatInfo.caseChat.name;
    } else {
      $scope.chat.title = 'Notas: ' + $scope.chatInfo.caseChat.name;
    }
    $scope.gotoBottom();
  };

  $rootScope.$on('image-loaded', function (event, args) {
    if (args.queue === 0) {
      $scope.gotoBottom();
    }
  });

  $scope.messages = $firebaseArray(query);
  $scope.imagesData = [];
  $scope.imagesToUpload = [];

  $ionicLoading.show({
    template: 'Cargando Mensajes...',
    showBackdrop: false,
    maxWidth: 0,
    duration: $rootScope.LOADER_TIME
  });

  $scope.timer = null;

  $scope.gotoBottom = function () {
    console.log('gotoBottom.----------');
    $ionicLoading.hide();
    $ionicScrollDelegate.$getByHandle('chat').resize();
    $timeout.cancel($scope.timer);
    $scope.timer = $timeout(function () {
      $ionicScrollDelegate.$getByHandle('chat').scrollBottom();
    }, 50);
  };

  $scope.messages.$loaded().then(function () {
    $scope.gotoBottom();
    firstLoad = true;
  });

  $scope.takePhoto = function () {
    CameraService.takePhoto().then(function () {

      var messageFirebase = {
        'messageText': '',
        'status': 1,
        'sender': parseInt($scope.user.id),
        'sender_name': ($scope.user.name + ' ' + $scope.user.lastname),
        'chatMessage': parseInt(chat),
        'createdAt': window.moment().format('DD/MM/YY HH:mm')
      };
      var messageJSON = {
        'messageText': '',
        'status': 1,
        'sender': parseInt($scope.user.id),
        'chatMessage': parseInt(chat)
      };

      $ionicLoading.show({
        template: 'Subiendo Imagen...',
        showBackdrop: false,
        maxWidth: 0,
        duration: $rootScope.LOADER_TIME
      });

      var notifData = {
        tipo: 'mensaje',
        caseId: $scope.chatInfo.caseChat.id,
        chatId: chat,
        priority: 2,
        notId: $scope.chat.id,
        style: 'inbox'
      };
      notifData['content-available'] = 1;
      notifData['force-start'] = 1;
      aidoAPIFactory.sendNotification($scope.otherUser.id, $scope.chat.title, 'Nueva fotografía', notifData);

      aidoAPIFactory.createMessage(messageJSON, $rootScope.csrfToken).then(function (response) {
        var path = 'msg_files/' + chat + '/' + response.data.id + '.jpg';
        messageFirebase.attachment = path;

        var promises = [];

        CameraService.imagesToUpload.forEach(function (im) {
          var defer = $q.defer();
          promises.push(defer.promise);
          AttachmentsService.uploadBase64(path, im).then(function () {
            $scope.messages.$add(messageFirebase);
            defer.resolve();
          });
        });

        $q.all(promises).then(function () {
          CameraService.imagesToUpload = [];
          $scope.gotoBottom();
        });

        $scope.chat.message = '';
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });

    });

  };

  $scope.choosePhoto = function () {
    CameraService.choosePhoto().then(function () {

      var messageFirebase = {
        'messageText': '',
        'status': 1,
        'sender': parseInt($scope.user.id),
        'sender_name': ($scope.user.name + ' ' + $scope.user.lastname),
        'chatMessage': parseInt(chat),
        'createdAt': window.moment().format('DD/MM/YY HH:mm')
      };
      var messageJSON = {
        'messageText': '',
        'status': 1,
        'sender': parseInt($scope.user.id),
        'chatMessage': parseInt(chat)
      };

      $ionicLoading.show({
        template: 'Subiendo Imagen...',
        showBackdrop: false,
        maxWidth: 0,
        duration: $rootScope.LOADER_TIME
      });

      var notifData = {
        tipo: 'mensaje',
        caseId: $scope.chatInfo.caseChat.id,
        chatId: chat,
        priority: 2,
        notId: $scope.chat.id,
        style: 'inbox'
      };
      notifData['content-available'] = 1;
      notifData['force-start'] = 1;
      aidoAPIFactory.sendNotification($scope.otherUser.id, $scope.chat.title, 'Nueva fotografía', notifData);

      aidoAPIFactory.createMessage(messageJSON, $rootScope.csrfToken).then(function (response) {
        var path = 'msg_files/' + chat + '/' + response.data.id + '.jpg';
        messageFirebase.attachment = path;

        var promises = [];

        CameraService.imagesToUpload.forEach(function (im) {
          var defer = $q.defer();
          promises.push(defer.promise);
          AttachmentsService.uploadBase64(path, im).then(function () {
            $scope.messages.$add(messageFirebase);
            defer.resolve();
          });
        });

        $q.all(promises).then(function () {
          CameraService.imagesToUpload = [];
          $scope.gotoBottom();
        });

        $scope.chat.message = '';
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });

    });

  };

  $scope.sendMessage = function (evt, message) {
    console.log(evt);
    // window.cordova.plugins.Keyboard.show();
    var notifData = {};
    if ($scope.tabIsChat) {
      var messageFirebase = {
        'messageText': message,
        'status': 1,
        'sender': parseInt($scope.user.id),
        'sender_name': ($scope.user.name + ' ' + $scope.user.lastname),
        'chatMessage': parseInt(chat),
        'createdAt': window.moment().format('DD/MM/YY HH:mm')
      };
      // $ionicScrollDelegate.$getByHandle('chat').scrollBottom(true);
      $scope.messages.$add(messageFirebase);
      notifData = {
        tipo: 'mensaje',
        caseId: $scope.chatInfo.caseChat.id,
        chatId: chat,
        priority: 2,
        'notId': '' + chat,
        style: 'inbox'
      };
      notifData['content-available'] = 1;
      notifData['force-start'] = 1;
      aidoAPIFactory.sendNotification($scope.otherUser.id, $scope.chat.title, message, notifData);
      // $ionicScrollDelegate.$getByHandle('chat').scrollBottom(true);
      // var messageJSON = {
      //   'messageText': message,
      //   'status': 1,
      //   'sender': parseInt($scope.user.id),
      //   'chatMessage': parseInt(chat)
      // };
      // aidoAPIFactory.createMessage(messageJSON, $rootScope.csrfToken).then(function () {
        // $ionicScrollDelegate.$getByHandle('chat').resize();
        // $ionicScrollDelegate.$getByHandle('chat').scrollBottom(true);
      //   $scope.messages.$add(messageFirebase);
      // }, function (error) {
      //   Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      // });
      // $scope.messages.$add(messageFirebase);
      $scope.gotoBottom();
      // window.$('.focusable').focus();
      $scope.chat.message = '';
      // window.$('.focusable').focus();
      // window.cordova.plugins.Keyboard.show();
    } else {
      var noteJSON = {
        'text': message,
        'sender': $scope.user,
        'type': 'public',
        'chatNote': parseInt(chat)
      };
      notifData = {
        tipo: 'nota',
        caseId: $scope.chatInfo.caseChat.id,
        chatId: chat,
        priority: 2,
        notId: '' + chat,
        style: 'inbox'
      };
      notifData['content-available'] = 1;
      notifData['force-start'] = 1;
      aidoAPIFactory.sendNotification($scope.otherUser.id, $scope.chat.title, message, notifData);

      aidoAPIFactory.createNote(noteJSON, $rootScope.csrfToken).then(function (response) {
        $scope.notes.push(response.data);
        $scope.chat.message = '';
        // $ionicScrollDelegate.$getByHandle('chat').scrollBottom(true);

      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });
    }

    $scope.chat.message = '';
  };

});
