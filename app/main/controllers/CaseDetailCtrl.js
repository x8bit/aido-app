'use strict';
angular.module('main')
.controller('CaseDetailCtrl', function ($scope, $stateParams, $rootScope, $http, $ionicLoading, $state, aidoAPIFactory, AuthService, $ionicScrollDelegate, $ionicModal, $ionicPopover, $ionicPopup, NotificationService) {
  $scope.activeUserId = AuthService.currentUserId();
  $scope.roleIsPatient = AuthService.isCurrentUserPatient();
  $scope.caso = {};
  $scope.caseId = parseInt($stateParams.id);

  $scope.status = '';
  $scope.vm = {
    title: 'Caso',
    showMore: false,
    btn: 'Más Info'
  };

  var scrollChanged = false;

  // $scope.showImages = function (index) {
  //   $scope.activeSlide = index;
  //   $scope.showModal('main/templates/image-popover.html');
  // };

  $scope.showImage = function (src) {
    $scope.imageSrc = src.target.currentSrc;
    window.PhotoViewer.show($scope.imageSrc);
  };
///popover
  $ionicPopover.fromTemplateUrl('main/templates/crearCasoPopOver.html', {
    scope: $scope
  }).then(function (popover) {
    $scope.popover = popover;
  });

  $scope.report = function () {
    $scope.popover.hide();
    $state.go('main.contacto');
    // alert('Gracias por reportar', null, 'Aido', 'Aceptar');
  };

  $scope.editarCaso = function () {
    $scope.popover.hide();
    $state.go('main.editarCaso', { caseId: $scope.caseId });
  };

  $scope.goToOptions = function ($event) {
    $scope.popover.show($event);
  };
////
  $scope.showModal = function (templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.goToHistory = function () {
    $scope.popover.hide();
    $state.go('main.casoHistory', { caseId: $scope.caseId });
  };

  $scope.checkBar = function () {
    if ($ionicScrollDelegate.getScrollPosition().top > 45 && !scrollChanged) {
      $scope.vm.title = $scope.caso.name;
      $scope.$apply();
      scrollChanged = true;
    } else if ($ionicScrollDelegate.getScrollPosition().top <= 45 && scrollChanged) {
      $scope.vm.title = 'Caso';
      $scope.$apply();
      scrollChanged = false;
    }
  };

  $scope.switchView = function () {
    $scope.vm.showMore = !$scope.vm.showMore;
    if ($scope.vm.showMore) {
      $ionicScrollDelegate.$getByHandle('detail').resize();
      $ionicScrollDelegate.$getByHandle('detail').scrollTop();
      $scope.vm.btn = 'Menos Info';
    } else {
      $ionicScrollDelegate.$getByHandle('detail').resize();
      $ionicScrollDelegate.$getByHandle('detail').scrollTop();
      $scope.vm.btn = 'Más Info';
    }
  };

  $scope.goToChat = function (chatId) {
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'chatRoom/' + chatId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'messageCount': 0
      }
    };
    $http(req).then(function () {
      var count = parseInt(NotificationService.getChatNotification(chatId));
      var old = parseInt(NotificationService.getNotification($scope.caseId));
      NotificationService.setNotification($scope.caseId, (old - count));
      NotificationService.clearChatNotification(chatId);

      aidoAPIFactory.updateCaseNotifications($scope.caseId, (old - count)).then(function () {
        console.log('actualizado');
      });
      $state.go('main.chat', {caseId: $scope.caseId, id: chatId});
    });
  };

  $scope.goToPrescription =  function (chat) {
    $state.go('main.consultarReceta', { medicId: chat.participants[0].id, caseTitle: $scope.caso.name , chatId: chat.id});
  };

  $rootScope.$on('image-loaded', function (event, args) {
    if (args.queue === 0) {
      $ionicScrollDelegate.$getByHandle('detail').resize();
    }
  });

  $scope.closeCase = function () {
    $scope.popover.hide();
    $ionicPopup.confirm({
      title: 'Aido',
      template: '¿Deseas cerrar el caso?',
      cancelText: 'Cancelar',
      okText: 'Aceptar'
    }).then(function (res) {
      if (res) {
        aidoAPIFactory.closeCase($scope.caseId).then(function () {
          $state.go('main.listaDeCasos');
        });
      }
    });
  };

  $scope.openCase = function () {
    $scope.popover.hide();
    $ionicPopup.confirm({
      title: 'Aido',
      template: '¿Deseas reabrir el caso?',
      cancelText: 'Cancelar',
      okText: 'Aceptar'
    }).then(function (res) {
      if (res) {
        aidoAPIFactory.openCase($scope.caseId).then(function () {
          $state.go('main.listaDeCasos');
        });
      }
    });
  };

  $ionicLoading.show({
    template: 'Cargando caso...',
    showBackdrop: false,
    maxWidth: 0,
    duration: $rootScope.LOADER_TIME
  }).then(function () {
    aidoAPIFactory.getCaseDetail($stateParams.id).then(function (data) {
      $scope.caso = data;
      $scope.popoverHeight = ($scope.caso.status === 'active') ? '225px' : '165px';

      var test = angular.copy($scope.caso.firebaseAttachments);
      $scope.arrOfArr = [];

      for (var i = 0; i < $scope.caso.firebaseAttachments.length; i++) {
        if ((i % 4 ) === 0) {
          $scope.arrOfArr.push(test.splice(0, 4));
        }
      }

      $scope.status = ($scope.caso.status === 'active') ? 'Caso Abierto' : 'Caso Cerrrado';
      // Quitar al paciente de los participantes del chat
      $scope.caso.chats.forEach(function (c) {
        console.log(c);
        if (!$scope.roleIsPatient) {
          if (typeof c.medicMessageCount !== 'undefined' && c.medicMessageCount > 0 ) {
            NotificationService.setChatNotification(c.id, c.medicMessageCount);
          }
          c.notifications = NotificationService.getChatNotification(c.id) || 0;
          c.participants = c.participants.filter(function (p) {
            p.onlineStatus = (p.onlineStatus) ? true : false;
            return p.id !== $scope.caso.patient;
          });
        } else {
          if (typeof c.patientMessageCount !== 'undefined' && c.patientMessageCount > 0 ) {
            NotificationService.setChatNotification(c.id, c.patientMessageCount);
          }
          c.notifications = NotificationService.getChatNotification(c.id) || 0;
          c.participants = c.participants.filter(function (p) {
            p.onlineStatus = (p.onlineStatus) ? true : false;
            return p.id !== $scope.caso.patient;
          });
        }
      });
      // Si es médico solo mostrar sus consultas
      // if (!$scope.roleIsPatient) {
      //   $scope.caso.chats.forEach(function (c) {
      //     // if (typeof c.messageCount !== 'undefined' && c.messageCount > 0 ) {
      //     //   NotificationService.setNotification(c.id, c.messageCount);
      //     // }
      //     c.notifications = NotificationService.getChatNotification(c.id) || 0;
      //     c.participants = c.participants.filter(function (p) {
      //       return p.id === $scope.activeUserId;
      //     });
      //   });
      // }
      $ionicLoading.hide();
    }, function () {
      $ionicLoading.hide();
    });
  });

  $scope.nuevaConsulta = function () {
    var objCase = {};
    objCase.id = $scope.caso.id;
    $state.go('main.seleccionarMedico', {caseId: objCase.id});
  };

  $scope.$on('onPushNotification', function (event, data) {
    console.log(event);
    console.log(data);
    $scope.caso.chats.forEach(function (c) {
      c.notifications = NotificationService.getChatNotification(c.id) || 0;
    });
    $scope.$apply();
  });

});
