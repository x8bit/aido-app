'use strict';
angular.module('main')
.controller('ProfileCtrl', function ($scope, $stateParams, AuthService, aidoAPIFactory, $rootScope, $ionicLoading, ionicDatePicker, $ionicModal, Utils) {
  $scope.user = AuthService.sessionInfo().session;
  console.log($scope.user);
  $scope.roleIsPatient = AuthService.isCurrentUserPatient();
  $scope.profile = {};
  $scope.config = {};
  $scope.comisionDoctor = 0;
  $scope.comisionAido = 0;

  $scope.tipoSangre = ['O+', 'O-', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-'];
  $scope.tomas = ['No', 'Menos de 2 veces al mes', '1 o mas veces por semana'];
  $scope.ejercicio = ['No hago ejercicio', 'Una vez por semana', 'Hasta tres veces por semana', 'Diario'];
  $scope.fumas = ['No', 'Menos de 5 cigarillos por semana', '1 a 5 cigarrillos al dia', '5 a 10 cigarrillos al dia', 'Más de 10 cigarrillos al dia'];

  $scope.caseImages = [];

  $scope.getComision = function () {
    if (!$scope.roleIsPatient) {
      if ($scope.user.rate === '' || parseInt($scope.user.rate) === 0) {
        $scope.user.rate = $scope.config.minRate;
      } else {
        $scope.user.rate = parseFloat($scope.user.rate);
      }
      var aido = $scope.user.rate * ($scope.config.aidoFee / 100);
      $scope.comisionAido = parseFloat(aido).toFixed(2);
      $scope.comisionDoctor = ($scope.user.rate - $scope.comisionAido).toFixed(2);
    }
  };

  if ($scope.roleIsPatient) {

    $scope.caseImages = [
      'main/assets/images/aido_avatar_user-20.png',
      'main/assets/images/aido_avatar_user-21.png',
      'main/assets/images/aido_avatar_user-22.png',
      'main/assets/images/aido_avatar_user-23.png',
      'main/assets/images/aido_avatar_user-24.png',
      'main/assets/images/aido_avatar_user-25.png',
      'main/assets/images/aido_avatar_user-26.png',
      'main/assets/images/aido_avatar_user-27.png',
      'main/assets/images/aido_avatar_user-28.png',
      'main/assets/images/aido_avatar_user-29.png',
      'main/assets/images/aido_avatar_user-30.png',
      'main/assets/images/aido_avatar_user-31.png',
      'main/assets/images/aido_avatar_user-32.png',
      'main/assets/images/aido_avatar_user-33.png',
      'main/assets/images/aido_avatar_user-34.png',
      'main/assets/images/aido_avatar_user-35.png',
      'main/assets/images/aido_avatar_user-36.png'
    ];

  } else {

    $scope.caseImages = [
      'main/assets/images/aido_avatar_doc-37.png',
      'main/assets/images/aido_avatar_doc-38.png',
      'main/assets/images/aido_avatar_doc-39.png',
      'main/assets/images/aido_avatar_doc-40.png',
      'main/assets/images/aido_avatar_doc-41.png',
      'main/assets/images/aido_avatar_doc-42.png',
      'main/assets/images/aido_avatar_doc-43.png',
      'main/assets/images/aido_avatar_doc-44.png',
      'main/assets/images/aido_avatar_doc-45.png',
      'main/assets/images/aido_avatar_doc-46.png',
      'main/assets/images/aido_avatar_doc-47.png',
      'main/assets/images/aido_avatar_doc-48.png',
      'main/assets/images/aido_avatar_doc-49.png',
      'main/assets/images/aido_avatar_doc-50.png',
      'main/assets/images/aido_avatar_doc-51.png',
      'main/assets/images/aido_avatar_doc-52.png'
    ];

  }

  var test = angular.copy($scope.caseImages);
  $scope.arrOfArr = [];

  for (var i = 0; i < $scope.caseImages.length; i++) {
    if ((i % 4 ) === 0) {
      $scope.arrOfArr.push(test.splice(0, 4));
    }
  }

  $scope.especialidades = {};
  $scope.especialidadesSeleccionadas = [];

  var profileId = 0;
  if ($scope.user.profile) {
    profileId = $scope.user.profile.id;
  }

  console.log(profileId);

  if (!$scope.roleIsPatient) {
    aidoAPIFactory.getSpecialties().then(function (response) {
      $scope.especialidades = response.data;
    });
  }

  $scope.specialtiesChange = function () {
    $scope.especialidadesSeleccionadas = [];
    $scope.especialidadesSeleccionadas.push($scope.profile.especialidades);
    // console.log($scope.profile.especialidades);
    // for (var i = 0; i < $scope.profile.especialidades.length; i++) {
    //   $scope.especialidadesSeleccionadas.push($scope.profile.especialidades[i]);
    // }
  };

  if (profileId !== 0) {
    console.log($scope.user);
    $ionicLoading.show({
      template: 'Obteniendo datos de perfil...',
      showBackdrop: false,
      maxWidth: 0,
      duration: $rootScope.LOADER_TIME
    }).then(function () {
      aidoAPIFactory.getProfile(profileId).then(function (response) {
        $scope.profile = response.data;
        $scope.profile.birthday = new Date($scope.profile.birthday);
        $scope.formatedDate = window.moment(new Date($scope.profile.birthday)).format('DD-MM-YYYY');
        $scope.especialidadesSeleccionadas = $scope.user.specialties;
        if ($scope.profile.defaultAvatar === null) {
          $scope.profile.defaultAvatar = 'main/assets/images/a_Icon_Perfil.png';
        }
        aidoAPIFactory.getConfig().then(function (c) {
          $scope.config = c.data[0];
          $scope.getComision();
        }, function (error) {
          Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
        });
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });
    });

  } else {

    $scope.profile.defaultAvatar = 'main/assets/images/a_Icon_Perfil.png';

  }

  $scope.openDatePicker = function () {
    ionicDatePicker.openDatePicker({
      callback: function (val) {
        $scope.profile.birthday = new Date(val);
        $scope.formatedDate = window.moment(new Date(val)).format('DD-MM-YYYY');
      },
      inputDate: new Date($scope.profile.birthday)
    });
  };

  $scope.selectImage = function (path) {
    $scope.profile.defaultAvatar = path;
    $scope.closeModal();
  };

  $scope.showImages = function () {
    $scope.showModal('main/templates/selectImageModal.html');
  };

  $scope.showModal = function (templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.save = function () {

    $ionicLoading.show({
      template: 'Guardando...',
      showBackdrop: false,
      maxWidth: 0,
      duration: $rootScope.LOADER_TIME
    }).then(function () {
      if (!$scope.roleIsPatient) {
        if (!$scope.user.rate || $scope.user.rate === '') {
          $scope.user.rate = 0;
        }
      }

      var specialties = [];
      var userJSON = window._.clone($scope.user);

      if (!$scope.roleIsPatient) {
        for (var i in $scope.especialidadesSeleccionadas) {
          specialties.push($scope.especialidadesSeleccionadas[i]);
        }
        userJSON.specialties = specialties;

        if (profileId !== 0) {
          aidoAPIFactory.addToUser($scope.user.id, userJSON, $rootScope.csrfToken).then(function () {
            aidoAPIFactory.addToProfile($scope.profile.id, $scope.profile, $rootScope.csrfToken).then(function (response) {
              $scope.profile = response.data;
              Utils.alert('Se guardaron los cambios con exito', null, 'Aido', 'Aceptar');
            }, function (error) {
              Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
            });
          }, function (error) {
            Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
          });

        } else {

          if ($scope.especialidadesSeleccionadas.length > 0) {

            aidoAPIFactory.createProfile($scope.profile).then(function (response) {
              var createdProfile = response.data;

              aidoAPIFactory.addToUser($scope.user.id, { 'profile': createdProfile.id, 'specialties': $scope.especialidadesSeleccionadas }, $rootScope.csrfToken).then(function () {
                AuthService.refreshSession(response.data);
                $scope.profile = response.data;
                Utils.alert('Se guardaron los cambios con exito', null, 'Aido', 'Aceptar');

              }, function (error) {
                Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
              });

            }, function (error) {
              Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
            });

          } else {
            Utils.alert('Debes seleccionar una especialidad', null, 'Aido', 'Aceptar');
          }

        }

      } else {

        if (profileId !== 0) {
          aidoAPIFactory.addToUser($scope.user.id, userJSON, $rootScope.csrfToken).then(function () {
            aidoAPIFactory.addToProfile($scope.profile.id, $scope.profile, $rootScope.csrfToken).then(function (response) {
              $scope.profile = response.data;
              $scope.profile.birthday = new Date($scope.profile.birthday);
              Utils.alert('Se guardaron los cambios con exito', null, 'Aido', 'Aceptar');
            }, function (error) {
              Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
            });
          }, function (error) {
            Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
          });

        } else {

          aidoAPIFactory.createProfile($scope.profile).then(function (response) {
            var createdProfile = response.data;

            aidoAPIFactory.addToUser($scope.user.id, { 'profile': createdProfile.id }, $rootScope.csrfToken).then(function () {
              AuthService.refreshSession(response.data);

              $scope.profile = response.data;
              $scope.profile.birthday = new Date($scope.profile.birthday);
              Utils.alert('Se guardaron los cambios con exito', null, 'Aido', 'Aceptar');

            }, function (error) {
              Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
            });

          }, function (error) {
            Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
          });

        }

      }

    });
  };

});
