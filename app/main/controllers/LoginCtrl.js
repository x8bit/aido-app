'use strict';
angular.module('main')
.controller('LoginCtrl', function ($scope, $state, AuthService, $rootScope, $ionicLoading, $ionicHistory, Utils) {
  $scope.loginInfo = {};
  $scope.loginForm = {};

  $scope.loginForm.email = '';
  $scope.loginForm.pass = '';

  $scope.submitForm = function (isValid) {
    if (isValid) {
      $scope.loginInfo.email = $scope.loginForm.email;
      $scope.loginInfo.password = $scope.loginForm.pass;

      $ionicLoading.show({
        template: 'Iniciando sesión...',
        showBackdrop: false,
        maxWidth: 0,
        duration: $rootScope.LOADER_TIME
      }).then(function () {
        AuthService.login($scope.loginInfo, $rootScope.csrfToken).then(function () {
          $scope.loginInfo.email = '';
          $scope.loginInfo.password = '';
          $scope.loginForm.email = '';
          $scope.loginForm.pass = '';
          $ionicHistory.clearCache().then(function () {
            $state.go('main.listaDeCasos');
          });

        }, function (error) {
          Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
        });
      });
    }
  };

});
