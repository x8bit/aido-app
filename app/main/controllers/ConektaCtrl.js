'use strict';
angular.module('main')
.controller('ConektaCtrl', function ($scope, $state, $stateParams, aidoAPIFactory, $ionicModal, Utils, $rootScope, $ionicLoading, $ionicPopup, $ionicHistory, AuthService) {
  var userId = $stateParams.userId;
  $scope.tokenParams = {
    card: {
      number: '',
      name: '',
      exp_year: '',
      exp_month: '',
      cvc: ''
    }
  };
  $scope.medic = $stateParams.medic;
  $scope.customer = {};
  $scope.modal = null;
  $scope.edit = false;
  $scope.hasCustomer = false;
  $scope.modalTitle = '';
  $scope.editTitle = '';
  var currentIndex = null;
  var messages = '';

  var conektaSuccessResponseHandler = function (token) {
    if ($scope.hasCustomer) {
      aidoAPIFactory.createPaymentSource($scope.customer.id, token.id).then(function () {
        $scope.getCustomerInfo();
        $scope.closeModal();
      });
    } else {
      aidoAPIFactory.createCustomer(userId, token.id).then(function () {
        $scope.getCustomerInfo();
        $scope.closeModal();
      });
    }
  };

  var conektaErrorResponseHandler = function (response) {
    console.log(response.message_to_purchaser);
  };

  var validate = function (params, num) {
    if (!window.Conekta.card.validateNumber(params.card.number) && num) {
      messages = 'Número de tarjeta invalido';
      return false;
    } else if (!window.Conekta.card.validateCVC(params.card.cvc)) {
      messages = 'Número CVC invalido';
      return false;
    } else if (!window.Conekta.card.validateExpirationDate(params.card.exp_month, params.card.exp_year)) {
      messages = 'Fecha de expiración invalida';
      return false;
    } else {
      return true;
    }
  };

  var getCardToken = function () {
    var params = window.angular.copy($scope.tokenParams);
    if (validate(params, true)) {
      window.Conekta.token.create(params, conektaSuccessResponseHandler, conektaErrorResponseHandler);
    } else {
      Utils.alert(messages, null, 'Aido', 'Aceptar');
    }
  };

  $scope.addCard = function () {
    getCardToken();
  };

  $scope.getCustomerInfo = function () {
    aidoAPIFactory.getCustomer(userId).then(function (customer) {
      if (customer && customer.id) {
        $scope.customer = customer;
        $scope.hasCustomer = true;
      } else {
        $scope.hasCustomer = false;
      }
    });
  };

  $scope.updateCard = function () {
    var cardInfo = {
      name: $scope.tokenParams.card.name,
      exp_year: $scope.tokenParams.card.exp_year,
      exp_month: $scope.tokenParams.card.exp_month,
      cvc: $scope.tokenParams.card.cvc
    };
    $ionicLoading.show({
      template: 'Actualizando...',
      showBackdrop: false,
      maxWidth: 0,
    });
    if (validate($scope.tokenParams, false)) {
      aidoAPIFactory.updatePaymentSource($scope.customer.id, cardInfo, currentIndex).then(function () {
        $scope.getCustomerInfo();
        $scope.closeModal();
        $ionicLoading.hide();
      });
    } else {
      $ionicLoading.hide();
      Utils.alert(messages, null, 'Aido', 'Aceptar');
    }
  };

  $scope.deleteCard = function () {
    $ionicPopup.confirm({
      title: 'Aido',
      template: '¿Deseas eliminar la tarjeta?',
      cancelText: 'Cancelar',
      okText: 'Aceptar'
    }).then(function (res) {
      if (res) {
        $ionicLoading.show({
          template: 'Eliminando...',
          showBackdrop: false,
          maxWidth: 0
        });
        aidoAPIFactory.deletePaymentSource($scope.customer.id, currentIndex).then(function () {
          $scope.getCustomerInfo();
          $scope.closeModal();
          $ionicLoading.hide();
        });
      }
    });
  };

  $scope.getCustomerInfo();

  $scope.showCardModal = function (mode, index) {
    if (mode === 1) {
      currentIndex = index;
      $scope.tokenParams.card.number = '';
      $scope.tokenParams.card.name = $scope.customer.payment_sources.data[0].name;
      $scope.tokenParams.card.exp_year = $scope.customer.payment_sources.data[0].exp_year;
      $scope.tokenParams.card.exp_month = $scope.customer.payment_sources.data[0].exp_month;
      $scope.tokenParams.card.cvc = '';
      $scope.edit = true;
      $scope.modalTitle = 'Editar tarjeta';
      $scope.editTitle = $scope.customer.payment_sources.data[0].brand + ' ' + $scope.customer.payment_sources.data[0].last4;
    } else {
      $scope.modalTitle = 'Aregar tarjeta';
      $scope.tokenParams = {
        card: {
          number: '',
          name: '',
          exp_year: '',
          exp_month: '',
          cvc: ''
        }
      };
      $scope.edit = false;
    }
    $ionicModal.fromTemplateUrl('main/templates/conektaCardModal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.pay = function () {
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    if ($scope.customer && $scope.customer.id !== '') {
      if (typeof $scope.customer.payment_sources !== 'undefined') {
        $ionicLoading.show({
          template: 'Pagando...',
          showBackdrop: false,
          maxWidth: 0
        });
        var transactionId = 0;
        aidoAPIFactory.createOrder(userId, $scope.customer.id, $scope.medic.id, $scope.customer.payment_sources.data[0].id).then(function (response) {
          var chatJSON = {};
          transactionId = response.transaction.id;
          $ionicLoading.hide();
          chatJSON.participants = [parseInt($scope.medic.id), parseInt(userId)];
          chatJSON.medicId = parseInt($scope.medic.id);
          chatJSON.caseChat = parseInt($stateParams.case);
          aidoAPIFactory.createChatRoom(chatJSON, $rootScope.csrfToken).then(function (response) {
            var chatId = response.data.id;
            aidoAPIFactory.updateTransaction(transactionId, chatId);
            var notifData = {
              tipo: 'caso',
              caseId: chatJSON.caseChat,
              chatId: chatId,
              priority: 2,
              notId: chatId,
              style: 'inbox'
            };
            notifData['content-available'] = 1;
            notifData['force-start'] = 1;
            var nameStr = (typeof AuthService.sessionInfo().session.name !== 'undefined') ? AuthService.sessionInfo().session.name : '';
            var lastnameStr = (typeof AuthService.sessionInfo().session.lastname !== 'undefined') ? AuthService.sessionInfo().session.lastname : '';
            aidoAPIFactory.sendNotification($scope.medic.id, 'Nueva consulta', 'Consulta iniciada con ' + nameStr + ' ' + lastnameStr, notifData);

            $state.go('main.chat', {id: chatId});
          });
        }, function (error) {
          Utils.alert(error.data.details[0].message, null, 'Aido', 'Aceptar');
        });
      } else {
        Utils.alert('Agrega la tarjeta para poder realizar el pago', null, 'Aido', 'Aceptar');
      }
    }
  };
});
