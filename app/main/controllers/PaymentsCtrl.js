'use strict';
angular.module('main')
.controller('PaymentsCtrl', function ($scope, $state, $stateParams, aidoAPIFactory, Utils, $rootScope, $ionicHistory, $ionicLoading, AuthService) {
  console.log($stateParams);
  var userId = $stateParams.userId;
  var medic = $stateParams.medic;
  $scope.medic = $stateParams.medic;
  var clientIDs = {
    'PayPalEnvironmentProduction': 'AQ_UJCRgTAh1o00xvPlTB08y9DRlqVGZnG26JMHGIUZeFpXr3TztC5F6H2I66jzVbzua0zDFd3WjAJXE',
    'PayPalEnvironmentSandbox': 'AblJeXoyjl95zrNXHv6NHQAJEQOMlklXrQ8ACrb2-ZGH5b9HCbh2MOjh-fYQllLY_Mm0jM3U7gRZQdd4'
  };
  var configuration = function () {
    var configOptions = {
      merchantName: 'Aido',
      merchantPrivacyPolicyURL: 'https://www.aido.mx/terminos-condiciones',
      merchantUserAgreementURL: 'https://www.aido.mx/terminos-condiciones',
      languageOrLocale: 'es_MX',
      acceptCreditCards: false
    };
    var _config = new window.PayPalConfiguration(configOptions);
    return _config;
  };

  var onPrepareRender = function () {
    console.log('======= onPrepareRender ==========');
  };

  var onPayPalMobileInit = function () {
    var payPalConfig = configuration();
    window.PayPalMobile.prepareToRender('PayPalEnvironmentProduction', payPalConfig, onPrepareRender);
  };

  var onSuccesfulPayment = function (payment) {
    console.log(payment);
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    $ionicLoading.show({
      template: 'Actualizando...',
      showBackdrop: false,
      maxWidth: 0,
    });

    var transactionId = 0;
    aidoAPIFactory.createTransaction(userId, medic.id, payment.response.id).then(function (response) {
      console.log(response);
      transactionId = response.transaction.id;
      $ionicLoading.hide();
      var chatJSON = {};
      chatJSON.participants = [parseInt($scope.medic.id), parseInt(userId)];
      chatJSON.medicId = parseInt($scope.medic.id);
      chatJSON.caseChat = parseInt($stateParams.case);
      aidoAPIFactory.createChatRoom(chatJSON, $rootScope.csrfToken).then(function (response) {
        var chatId = response.data.id;
        aidoAPIFactory.updateTransaction(transactionId, chatId);
        var notifData = {
          tipo: 'caso',
          caseId: chatJSON.caseChat,
          chatId: chatId,
          priority: 2,
          notId: chatId,
          style: 'inbox'
        };
        notifData['content-available'] = 1;
        notifData['force-start'] = 1;
        var nameStr = (typeof AuthService.sessionInfo().session.name !== 'undefined') ? AuthService.sessionInfo().session.name : '';
        var lastnameStr = (typeof AuthService.sessionInfo().session.lastname !== 'undefined') ? AuthService.sessionInfo().session.lastname : '';
        aidoAPIFactory.sendNotification($scope.medic.id, 'Nueva consulta', 'Consulta iniciada con ' + nameStr + ' ' + lastnameStr, notifData);
        $state.go('main.chat', {id: chatId});
      });
    }, function (error) {
      Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
    });
  };

  var onUserCanceled = function (result) {
    console.log(result);
  };

  var createPayment = function () {
    var total = parseFloat(medic.rate).toFixed(2);
    var paymentDetails = new window.PayPalPaymentDetails(total, '0.00', '0.00');
    var nameStr = (typeof medic.name !== 'undefined') ? medic.name : '';
    var lastnameStr = (typeof medic.lastname !== 'undefined') ? medic.lastname : '';
    var payment = new window.PayPalPayment(total, 'MXN', 'Consulta - ' + (nameStr + ' ' + lastnameStr), 'Sale', paymentDetails);
    return payment;
  };

  $scope.payWithPaypal = function () {
    window.PayPalMobile.renderSinglePaymentUI(createPayment(), onSuccesfulPayment, onUserCanceled);
  };

  $scope.payWithConekta = function () {
    $state.go('main.conekta', {
      userId: $stateParams.userId,
      medic: $stateParams.medic,
      case: $stateParams.case
    });
  };

  window.PayPalMobile.init(clientIDs, onPayPalMobileInit);
});
