'use strict';
angular.module('main')
.controller('HistoryCtrl', function ($scope, $stateParams, aidoAPIFactory, $ionicLoading, $rootScope, AuthService) {
  var caseId = parseInt($stateParams.caseId);
  $scope.user = AuthService.sessionInfo().session;
  $scope.roleIsPatient = AuthService.isCurrentUserPatient();
  $scope.caseInfo = {};
  $scope.history = [];

  $ionicLoading.show({
    template: 'Cargando Historial...',
    showBackdrop: false,
    maxWidth: 0,
    duration: $rootScope.LOADER_TIME
  }).then(function () {
    aidoAPIFactory.getHistory(caseId).then(function (response) {
      $scope.caseInfo = response.data[0].case;
      $scope.history = response.data.map(function (h) {
        if (h.event === 'chat-updated') {
          h.description = JSON.parse(h.description);
        }
        return h;
      });
      aidoAPIFactory.getUser($scope.caseInfo.patient).then(function (response) {
        $scope.caseInfo.patient = response.data;
      });
      $ionicLoading.hide();
    }, function () {
      $ionicLoading.hide();
    });
  });

  // 'description-changed','participant-added', 'chat-ended', 'prescription-added', 'note-added', 'message-added'
  //listo  'symptoms-added', 'chat-started'
  //falta
});
