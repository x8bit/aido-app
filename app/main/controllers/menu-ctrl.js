'use strict';
angular.module('main')
.controller('MenuCtrl', function ($scope, $state, AuthService, aidoAPIFactory) {
  $scope.menuOptions = [];
  $scope.currentUser = AuthService.sessionInfo().session;
  $scope.roleIsPatient = AuthService.isCurrentUserPatient();
  $scope.onlineStatus = ($scope.currentUser.onlineStatus) ? true : false;

  if (AuthService.isAuthenticated()) {
    if (AuthService.sessionInfo().session.role[0].id === 2) {
      $scope.menuOptions = [{
        icon: 'main/assets/images/aido_iconMenu_plus.png',
        title: 'Crear caso',
        state: 'main.crearCaso'
      }, {
        icon: 'main/assets/images/aido_iconMenu_list.png',
        title: 'Mis casos',
        state: 'main.listaDeCasos',
        notifications: 0
      }, {
        icon: 'main/assets/images/aido_iconMenu_user.png',
        title: 'Perfil',
        state: 'main.editProfile'
      }];
    } else {
      $scope.menuOptions = [{
        icon: 'main/assets/images/aido_iconMenu_list.png',
        title: 'Mis casos',
        state: 'main.listaDeCasos',
        notifications: 0
      }, {
        icon: 'main/assets/images/aido_iconMenu_user.png',
        title: 'Perfil',
        state: 'main.editProfile'
      }];
    }
  }

  $scope.termsAndConditions = function () {
    window.cordova.InAppBrowser.open('https://www.aido.mx/terminos-condiciones', '_system', 'location=yes');
  };

  $scope.logout = function () {
    AuthService.logout();
    $state.go('login');
  };

  $scope.switchStatus = function () {
    aidoAPIFactory.updateOnlineStatus($scope.currentUser.id, !$scope.onlineStatus).then(function (response) {
      $scope.onlineStatus = response.data.onlineStatus;
      AuthService.refreshOnlineStatus($scope.onlineStatus);
    });
  };

});
