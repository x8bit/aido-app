'use strict';
angular.module('main')
.controller('RecoverCtrl', function ($scope, $stateParams, aidoAPIFactory, Utils) {
  $scope.contact = {
    email: ''
  };

  $scope.submitForm = function (value) {
    if (value) {
      aidoAPIFactory.recoverPassword($scope.contact.email).then(function () {
        Utils.alert('Enviado con éxito', null, 'Aido', 'Aceptar');
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });
    }
  };

});
