'use strict';
angular.module('main')
.controller('ContactoCtrl', function ($scope, $stateParams, $ionicHistory, AuthService, aidoAPIFactory, Utils) {

  $ionicHistory.nextViewOptions({
    disableBack: true
  });
  $scope.user = AuthService.sessionInfo().session;

  $scope.contact = {};

  $scope.submitForm = function (value) {
    if (value) {
      $scope.contact.email = $scope.user.email;
      aidoAPIFactory.sendReport($scope.contact).then(function () {
        Utils.alert('Enviado con éxito', null, 'Aido', 'Aceptar');
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });
    }
  };

});
