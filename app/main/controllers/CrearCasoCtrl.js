'use strict';
angular.module('main')
.controller('CrearCasoCtrl', function ($scope, $stateParams, aidoAPIFactory, $rootScope, AuthService, $state, $ionicLoading, CameraService, AttachmentsService, $ionicPopup, $ionicModal, ionicDatePicker, Utils) {
  var activeUserId = AuthService.sessionInfo().session.id;

  $scope.newCaso = {};
  $scope.newCaso.defaultAvatar = 'main/assets/images/aido_icon_NuevoCasoL.png';
  $scope.newCaso.patient = activeUserId;
  $scope.newCaso.status = 'active';
  $scope.newCaso.symptoms = [];
  $scope.caseImages = [
    'main/assets/images/aido_icon_NuevoCasoL.png',
    'main/assets/images/aido_avatar_caso-53.png',
    'main/assets/images/aido_avatar_caso-54.png',
    'main/assets/images/aido_avatar_caso-55.png',
    'main/assets/images/aido_avatar_caso-56.png',
    'main/assets/images/aido_avatar_caso-57.png',
    'main/assets/images/aido_avatar_caso-58.png',
    'main/assets/images/aido_avatar_caso-59.png',
    'main/assets/images/aido_avatar_caso-60.png',
    'main/assets/images/aido_avatar_caso-61.png',
    'main/assets/images/aido_avatar_caso-62.png',
    'main/assets/images/aido_avatar_caso-63.png',
    'main/assets/images/aido_avatar_caso-64.png',
    'main/assets/images/aido_avatar_caso-65.png',
    'main/assets/images/aido_avatar_caso-66.png'
  ];
  var test = angular.copy($scope.caseImages);
  $scope.arrOfArr = [];

  for (var i = 0; i < $scope.caseImages.length; i++) {
    if ((i % 4 ) === 0) {
      $scope.arrOfArr.push(test.splice(0, 4));
    }
  }

  $scope.openDatePicker = function () {
    ionicDatePicker.openDatePicker({
      callback: function (val) {
        $scope.newCaso.symptomsDate = new Date(val);
        $scope.formatedDate = window.moment(new Date(val)).format('DD-MM-YYYY');
      }
    });
  };

  $scope.symptomsAdded = [];

  $scope.setFlag = function (data) {
    aidoAPIFactory.createSymptom(data).then(function () {
      aidoAPIFactory.getSymptoms().then(function (response) {
        $scope.symptomsAdded = response.data;
      });
    });
  };

  $scope.attachment = {};
  $scope.symptomsList = [];

  aidoAPIFactory.getSymptoms().then(function (response) {
    $scope.symptomsList = response.data;
  });

  $scope.symptomsSelected = [];

  $scope.symptomsChange = function () {
    $scope.symptomsSelected = [];
    for (var i = 0; i < $scope.newCaso.symptoms.length; i++) {
      $scope.symptomsSelected.push($scope.newCaso.symptoms[i]);
    }
  };

  $scope.imagesData = [];
  $scope.imagesToUpload = [];

  function guid () {
    function s4 () {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  $scope.selectImage = function (path) {
    $scope.newCaso.defaultAvatar = path;
    $scope.closeModal();
  };

  $scope.showImages = function () {
    $scope.showModal('main/templates/selectImageModal.html');
  };

  $scope.showImagesPreview = function (index) {
    $scope.activeSlide = index;
    $scope.showModal('main/templates/previewImageModal.html');
  };

  $scope.showModal = function (templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.submitForm = function (isValid) {
    if (isValid) {

      $ionicLoading.show({
        template: 'Creando caso...',
        showBackdrop: false,
        maxWidth: 0,
        duration: $rootScope.LOADER_TIME
      }).then(function () {

        $scope.newCaso.symptoms = $scope.newCaso.symptoms.map(function (s) {
          if (typeof s.id === 'string') {
            var name = s.id.split('-')[0];
            var symptom = $scope.symptomsAdded.filter(function (s) {
              return s.name === name;
            })[0];

            return symptom.id;
          } else {
            return s.id;
          }
        });

        aidoAPIFactory.createCase($scope.newCaso, $rootScope.csrfToken).then(function (response) {
          var newCase = response.data;
          $scope.newCaso = {};
          var attachmentsArray = [];

          CameraService.imagesToUpload.forEach(function (im) {
            var path = 'case_files/' + newCase.id + '/' + guid();
            attachmentsArray.push(path);
            AttachmentsService.uploadBase64(path, im);
          });

          aidoAPIFactory.updateCaseAttachments(newCase.id, attachmentsArray).then(function () {
            CameraService.imagesToUpload = [];
          });

          $state.go('main.seleccionarMedico', { caseId: newCase.id });
        }, function (error) {
          Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
        });

      });
    }
  };


  $scope.showAttachmentPopup = function () {
    $ionicPopup.show({
      template: '',
      cssClass: 'parentPop',
      title: 'Que deseas hacer?',
      scope: $scope,
      buttons: [
        {
          type: 'icon ion-ios-camera-outline',
          onTap: function () {
            $scope.takePhoto();
          }
        },
        {
          type: 'icon ion-paperclip',
          onTap: function () {
            $scope.choosePhoto();
          }
        },
        {
          type: 'icon ion-close',
        }
      ]
    });
  };

  $scope.eliminatePhoto = function (index) {
    $scope.imagesData.splice(index, 1);
    $scope.imagesToUpload.splice(index, 1);
  };

  $scope.takePhoto = function () {
    CameraService.takePhoto().then(function (base64String) {
      $scope.imagesData.push(base64String);
    });
  };

  $scope.choosePhoto = function () {
    CameraService.choosePhoto().then(function (base64String) {
      $scope.imagesData.push(base64String);
    });
  };

});
