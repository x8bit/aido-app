'use strict';
angular.module('main')
.controller('BienvenidaCtrl', function ($scope, $stateParams, $ionicHistory) {

  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  $scope.newUserId = $stateParams.newUserId;
  $scope.newRoleId = $stateParams.newRoleId;
  $scope.selectedAvatar = $stateParams.avatar;

});
