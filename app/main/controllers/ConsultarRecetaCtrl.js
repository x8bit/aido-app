'use strict';
angular.module('main')
.controller('ConsultarRecetaCtrl', function ($scope, $stateParams, aidoAPIFactory, $ionicLoading, $rootScope, $ionicModal) {
  $scope.medicId = $stateParams.medicId;
  $scope.chatId = $stateParams.chatId;
  $scope.caseTitle = $stateParams.caseTitle;

  $ionicModal.fromTemplateUrl('main/templates/detalleMedicoModal.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function (modal) {
    $scope.medicModal = modal;
  });

  $scope.openMedicModal = function () {
    $scope.medicModal.show();
  };

  $scope.$on('$destroy', function () {
    $scope.medicModal.remove();
  });

  $ionicLoading.show({
    template: 'Cargando Receta...',
    showBackdrop: false,
    maxWidth: 0,
    duration: $rootScope.LOADER_TIME
  }).then(function () {

    aidoAPIFactory.getPrescription($scope.medicId, $scope.chatId).then(function (response) {
      $scope.prescriptions = response.data.prescriptions;
      $scope.otherUser = response.data.medic;
    });
  });

});
