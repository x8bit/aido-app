'use strict';
angular.module('main')
.controller('InviteCtrl', function ($scope, $stateParams, aidoAPIFactory, $ionicLoading, AuthService,  $ionicPopup, $state, $ionicHistory) {

  $scope.user = AuthService.sessionInfo().session;
  $scope.invite = {};

  $scope.showAlert = function (title, msg) {
    $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  $scope.invite = function () {
    aidoAPIFactory.getUserbyEmail($scope.invite.email).then(function (response) {
      if (response.data.length !== 0) {
        $scope.showalert('Usuario Existente!', 'Se le ha enviado una invitacion!');
        $state.go('main.listaDeCasos');
      } else {
        $scope.showalert('Nuevo Usuario!', 'Se le ha enviado una invitacion!');
        $state.go('main.listaDeCasos');
      }
    });
  };

});
