'use strict';
angular.module('main')
.controller('PerfilMedicoCtrl', function ($scope, $stateParams, aidoAPIFactory, $rootScope, $state, AuthService, $ionicLoading, Utils) {
  var currentUserId = $stateParams.userId;
  var previousProfileInfo = JSON.parse($stateParams.profileJSON);
  $scope.profileForm = window._.clone(previousProfileInfo);

  $scope.tipoSangre = ['O+', 'O-', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-'];
  $scope.tomas = ['No', 'Menos de 2 veces al mes', '1 o mas veces por semana'];
  $scope.ejercicio = ['No hago ejercicio', 'Una vez por semana', 'Hasta tres veces por semana', 'Diario'];
  $scope.fumas = ['No', 'Menos de 5 cigarillos por semana', '1 a 5 cigarrillos al dia', '5 a 10 cigarrillos al dia', 'Más de 10 cigarrillos al dia'];

  $scope.submitForm = function () {
    // if ($scope.profileForm.smokes) {
    //   $scope.profileForm.smokes = $scope.profileForm.smokes.value;
    // }
    // if ($scope.profileForm.drinks) {
    //   $scope.profileForm.drinks = $scope.profileForm.drinks.value;
    // }
    // if ($scope.profileForm.excersiseFrequence) {
    //   $scope.profileForm.excersiseFrequence = $scope.profileForm.excersiseFrequence.value;
    // }
    $ionicLoading.show({
      template: 'Creando el perfil de la cuenta...',
      showBackdrop: false,
      maxWidth: 0,
      duration: $rootScope.LOADER_TIME
    }).then(function () {
      aidoAPIFactory.createProfile($scope.profileForm).then(function (response) {
        var createdProfile = response.data;

        aidoAPIFactory.addToUser(currentUserId, { 'profile': createdProfile.id }, $rootScope.csrfToken).then(function (response) {
          AuthService.refreshSession(response.data.profile);
          $state.go('main.listaDeCasos');

        }, function (error) {
          Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
        });

      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });

    });
  };

});
