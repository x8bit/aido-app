'use strict';
angular.module('main')
.controller('DatosMedicosCtrl', function ($scope, $stateParams, $state, aidoAPIFactory, $rootScope, $ionicLoading, Utils) {
  var currentUserId = $stateParams.userId;
  var avatar = $stateParams.avatar;

  $scope.profileForm = {};
  $scope.especialidades = {};
  $scope.especialidadesSeleccionadas = [];
  $scope.especialidadesIDS = [];
  $scope.avatar = avatar;

  aidoAPIFactory.getSpecialties().then(function (response) {
    $scope.especialidades = response.data;
  });

  $scope.specialtiesChange = function () {
    $scope.especialidadesSeleccionadas = [];
    $scope.especialidadesSeleccionadasIDS = [];
    $scope.especialidadesSeleccionadas.push($scope.profileForm.especialidades);
    $scope.especialidadesSeleccionadasIDS.push($scope.profileForm.especialidades.id);
    // for (var i = 0; i < $scope.profileForm.especialidades.length; i++) {
    //   $scope.especialidadesSeleccionadas.push($scope.profileForm.especialidades[i]);
    //   $scope.especialidadesSeleccionadasIDS.push($scope.profileForm.especialidades[i].id);
    // }
  };

  $scope.submitMedicoForm = function (isValid) {
    if (isValid) {
      if ($scope.especialidadesSeleccionadasIDS.length > 0) {
        $ionicLoading.show({
          template: 'Creando el perfil de la cuenta...',
          showBackdrop: false,
          maxWidth: 0,
          duration: $rootScope.LOADER_TIME
        }).then(function () {
          if (avatar && avatar !== '') {
            $scope.profileForm.defaultAvatar = avatar;
          } else {
            $scope.profileForm.defaultAvatar = 'main/assets/images/aido_avatar_doc-46.png';
          }
          aidoAPIFactory.createProfile($scope.profileForm).then(function (response) {
            var createdProfile = response.data;

            aidoAPIFactory.addToUser(currentUserId, { 'profile': createdProfile.id, 'specialties': $scope.especialidadesSeleccionadasIDS }, $rootScope.csrfToken).then(function () {
              $state.go('bienvenidaMedico');
            }, function (error) {
              Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
            });

          }, function (error) {
            Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
          });
        });
      } else {
        Utils.alert('Debes seleccionar una especialidad', null, 'Aido', 'Aceptar');
      }
    }
  };

});
