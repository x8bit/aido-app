'use strict';
angular.module('main')
.controller('RecetaCtrl', function ($scope, $stateParams, aidoAPIFactory, $ionicLoading, $rootScope, $ionicScrollDelegate, AuthService, Utils) {
  var otherUserId = $stateParams.otherUser;
  var chatId = $stateParams.chat;
  // var caseId = $stateParams.caseId;

  $scope.prescription = {};
  $scope.prescription.items = [];
  $scope.age = 0;
  $scope.otherUser = {};
  $scope.medicines = [];

  $scope.meds = {};
  var isNewMedicine = false;

  $scope.setFlag = function () {
    isNewMedicine = true;
  };

  $scope.addMedicine = function (newValue) {
    if (isNewMedicine) {
      // regresar la lista completa de medicinas en el create ?
      aidoAPIFactory.createMedicine(newValue.name).then(function () {
        aidoAPIFactory.getMedicines().then(function (response) {
          $scope.medicines = response.data;
          isNewMedicine = false;
        });
      });
    }
  };

  $ionicLoading.show({
    template: 'Cargando...',
    showBackdrop: false,
    maxWidth: 0,
    duration: $rootScope.LOADER_TIME
  }).then(function () {
    aidoAPIFactory.getUser(otherUserId).then(function (response) {
      $scope.otherUser = response.data;
      $scope.age = window.moment($scope.otherUser.profile.birthday).fromNow(true);
      $scope.age = $scope.age.replace(/\D/g, '');
    });

    aidoAPIFactory.getMedicines().then(function (response) {
      $scope.medicines = response.data;
    });

    aidoAPIFactory.getChatRoom(chatId).then(function (response) {
      $scope.oldChat = response.data;
    });
  });

  $scope.addRecipe = function () {
    var childScope = $scope.$new();
    childScope.item = {};
    childScope.item.dose = '';
    childScope.item.indication = '';
    childScope.item.selected = {};
    $scope.prescription.items.push(childScope.item);
    $ionicScrollDelegate.$getByHandle('detail').resize();
    $ionicScrollDelegate.$getByHandle('detail').scrollBottom(true);
  };

  $scope.removeRecipe = function (idx) {
    $scope.prescription.items.splice(idx, 1);
  };

  $scope.newPrescription = function () {
    $ionicLoading.show({
      template: 'Cargando...',
      showBackdrop: false,
      maxWidth: 0,
      duration: $rootScope.LOADER_TIME
    }).then(function () {
      for (var i = 0; i < $scope.prescription.items.length; i++) {
        if ($scope.prescription.items[i].selected.id === -1) {
          $scope.prescription.items[i].medicines = $scope.medicines.filter(function (m) { return m.name === $scope.prescription.items[i].selected.name; })[0];
        } else {
          $scope.prescription.items[i].medicines = $scope.prescription.items[i].selected;
        }
      }
      $scope.prescription.chatPrescription = chatId;
      $scope.prescription.medic = AuthService.currentUserId();
      aidoAPIFactory.createPrescription($scope.prescription).then(function (response) {
        var newPrescription = response.data;
        $scope.prescription.items = [];
        // var CaseHistoryJSON = {
        //   'case': caseId,
        //   'event': 'prescription-added',
        //   'prescription': newPrescription
        // };
        // aidoAPIFactory.createCaseHistory(CaseHistoryJSON, $localStorage.token).then( function (response) {
        //   console.log(response);
        // });
        var notifData = {
          tipo: 'receta',
          caseId: $scope.oldChat.caseChat,
          chatId: chatId,
          priority: 2,
          notId: chatId,
          style: 'inbox'
        };
        notifData['content-available'] = 1;
        notifData['force-start'] = 1;
        aidoAPIFactory.sendNotification($scope.medic.id, 'Nueva receta', 'Dr(a). ' + AuthService.sessionInfo().session.name + ' ' + AuthService.sessionInfo().lastname, notifData);

        aidoAPIFactory.addPrescriptionToChat(chatId, newPrescription.id).then(function () {
          Utils.alert('Se envio la receta con exito', null, 'Aido', 'Aceptar');
        }, function (error) {
          Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
        });
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });
    });
  };

  $scope.addRecipe();

});
