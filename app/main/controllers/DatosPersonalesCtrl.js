'use strict';
angular.module('main')
.controller('DatosPersonalesCtrl', function ($scope, $stateParams, $state, ionicDatePicker) {
  var currentUserId = $stateParams.userId;
  var avatar = $stateParams.avatar;

  $scope.profileForm = {};
  $scope.sexes = [{
    'nombre': 'Masculino',
    'value': 'M'
  }, {
    'nombre': 'Femenino',
    'value': 'F'
  }];

  $scope.openDatePicker = function () {
    ionicDatePicker.openDatePicker({
      callback: function (val) {
        $scope.profileForm.birthday = new Date(val);
        $scope.formatedDate = window.moment(new Date(val)).format('DD-MM-YYYY');
      }
    });
  };

  $scope.profileForm.sex = $scope.sexes[0];

  $scope.submitForm = function (isValid) {
    if (isValid) {
      $scope.profileForm.bodyMassIndex = -1;
      $scope.profileForm.sex = $scope.profileForm.sex.value;
      if ($scope.profileForm.height && $scope.profileForm.weight) {
        $scope.profileForm.bodyMassIndex = $scope.profileForm.weight / ($scope.profileForm.height * $scope.profileForm.height);
      }
      if (avatar && avatar !== '') {
        $scope.profileForm.defaultAvatar = avatar;
      } else {
        $scope.profileForm.defaultAvatar = ($scope.profileForm.sex === 'M') ? 'main/assets/images/aido_avatar_user-28.png' : 'main/assets/images/aido_avatar_user-20.png';
      }
      $state.go('perfilMedico', { userId: currentUserId, profileJSON: JSON.stringify($scope.profileForm) });
    }
  };

});
