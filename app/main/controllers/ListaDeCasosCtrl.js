'use strict';
angular.module('main')
.controller('ListaDeCasosCtrl', function ($rootScope, $scope, $state, $stateParams, AuthService, aidoAPIFactory, $ionicLoading, $ionicModal, NotificationService, Utils, $http) {
  var activeUserId = AuthService.sessionInfo().session.id;
  $scope.roleIsPatient = (AuthService.sessionInfo().session.role[0].id === 2) ? true : false;
  $scope.casos = {};
  $scope.casos.lista = [];
  $scope.status = 'active';

  $scope.inputs = {
    search: '',
    showMessage: false
  };

  $scope.results = [];

  $scope.openInvitationModal = function () {
    $scope.inputs.showMessage = false;
    $scope.inputs.search = '';
    $scope.results = [];
    $scope.modal.show();
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
  };

  $scope.$on('$destroy', function () {
    $scope.modal.remove();
  });

  $scope.searchUser = function () {
    if ($scope.inputs.search !== AuthService.sessionInfo().session.email) {

      aidoAPIFactory.searchByEmail($scope.inputs.search).then(function (results) {
        $scope.inputs.showMessage = (results.length === 0) ? true : false;
        $scope.results = results;
      });

    } else {
      Utils.alert('No puedes invitarte a ti mismo', null, 'Aido', 'Aceptar');
    }
  };

  $scope.invite = function (user) {
    $scope.inputs = {
      search: '',
      showMessage: false
    };
    aidoAPIFactory.sendInvitation(user.email, AuthService.sessionInfo().session.email).then(function () {
      Utils.alert('Invitación enviada', null, 'Aido', 'Aceptar');
    });
  };

  $scope.inviteNew = function () {
    aidoAPIFactory.sendNewInvitation($scope.inputs.search, AuthService.sessionInfo().session.email).then(function () {
      $scope.inputs = {
        search: '',
        showMessage: false
      };
      Utils.alert('Invitación enviada', null, 'Aido', 'Aceptar');
    });
  };

  $scope.dictionary = {
    'active': 'activos',
    'closed': 'cerrados'
  };

  var clearLoaders = function () {
    $ionicLoading.hide();
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.goToCrearCaso = function () {
    $state.go('main.crearCaso');
  };

  $scope.doRefresh = function () {
    $scope.getCases($scope.status);
  };

  $scope.view = function (caseObject) {
    if ($scope.roleIsPatient) {
      $state.go('main.casoDetail', {id: caseObject.id});
    } else {
      var req = {
        method: 'PUT',
        url: $rootScope.api_url + 'case/' + caseObject.id,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'badge': 0
        }
      };
      $http(req).then(function (response) {
        console.log('REsponssssssss', response.data);
        NotificationService.clearNotification(caseObject.id);
        $state.go('main.chat', {caseId: caseObject.id, id: caseObject.chatId});
      });
    }
  };

  $scope.getCases = function (status) {
    $scope.status = status;
    $ionicLoading.show({
      template: 'Cargando...',
      showBackdrop: false,
      maxWidth: 0
    }).then(function () {
      if ($scope.roleIsPatient) {
        aidoAPIFactory.getCases(activeUserId, $scope.status).then(function (response) {
          $scope.casos.lista = response.data.map(function (c) {
            if (typeof c.patientBadge !== 'undefined' && c.patientBadge > 0 ) {
              NotificationService.setNotification(c.id, c.patientBadge);
            }
            c.notifications = NotificationService.getNotification(c.id) || 0;
            return c;
          });
          clearLoaders();
        }, function () {
          clearLoaders();
        });
      } else {
        aidoAPIFactory.getMedicCases(activeUserId, $scope.status).then(function (response) {
          $scope.casos.lista = response.data.map(function (c) {
            if (typeof c.medicBadge !== 'undefined' && c.medicBadge > 0 ) {
              NotificationService.setNotification(c.id, c.medicBadge);
            }
            c.notifications = NotificationService.getNotification(c.id) || 0;
            return c;
          });
          clearLoaders();
        }, function () {
          clearLoaders();
        });
      }
    });

  };

  $scope.$on('onPushNotification', function (event, data) {
    console.log(event);
    console.log(data);
    var caso = $scope.casos.lista.filter(function (caso) { return caso.id === parseInt(data.caseId); });
    if (caso.length) {
      caso[0].notifications = NotificationService.getNotification(data.caseId);
      $scope.$apply();
    }
    $scope.doRefresh();
  });

  $scope.onKeyUp = function (event) {
    if (event.keyCode === 13) {
      $scope.searchUser();
    }
  };

  $scope.getCases('active');

  $ionicModal.fromTemplateUrl('main/templates/invitationModal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function (modal) {
    $scope.modal = modal;
  });

});
