'use strict';
angular.module('main')
.controller('RegistroCtrl', function ($scope, $stateParams, aidoAPIFactory, AuthService, $rootScope, $state, $ionicHistory, $ionicLoading, $ionicModal, Utils) {
  $scope.newUserInfo = {};
  $scope.registerForm = {};
  $scope.roles = [];
  $scope.registerForm.role = {};
  $scope.extra = {};
  $scope.extra.defaultAvatar = 'main/assets/images/aido_avatar_user-23.png';

  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  $scope.caseImages = [];

  aidoAPIFactory.getRoles().then(function (response) {
    $scope.roles = response.data;
    $scope.registerForm.role = $scope.roles[0];
    $scope.switchImages();
  });

  $scope.switchImages = function () {
    if ($scope.registerForm.role.id === 2) {

      $scope.caseImages = [
        'main/assets/images/aido_avatar_user-20.png',
        'main/assets/images/aido_avatar_user-21.png',
        'main/assets/images/aido_avatar_user-22.png',
        'main/assets/images/aido_avatar_user-23.png',
        'main/assets/images/aido_avatar_user-24.png',
        'main/assets/images/aido_avatar_user-25.png',
        'main/assets/images/aido_avatar_user-26.png',
        'main/assets/images/aido_avatar_user-27.png',
        'main/assets/images/aido_avatar_user-28.png',
        'main/assets/images/aido_avatar_user-29.png',
        'main/assets/images/aido_avatar_user-30.png',
        'main/assets/images/aido_avatar_user-31.png',
        'main/assets/images/aido_avatar_user-32.png',
        'main/assets/images/aido_avatar_user-33.png',
        'main/assets/images/aido_avatar_user-34.png',
        'main/assets/images/aido_avatar_user-35.png',
        'main/assets/images/aido_avatar_user-36.png'
      ];

    } else {

      $scope.caseImages = [
        'main/assets/images/aido_avatar_doc-37.png',
        'main/assets/images/aido_avatar_doc-38.png',
        'main/assets/images/aido_avatar_doc-39.png',
        'main/assets/images/aido_avatar_doc-40.png',
        'main/assets/images/aido_avatar_doc-41.png',
        'main/assets/images/aido_avatar_doc-42.png',
        'main/assets/images/aido_avatar_doc-43.png',
        'main/assets/images/aido_avatar_doc-44.png',
        'main/assets/images/aido_avatar_doc-45.png',
        'main/assets/images/aido_avatar_doc-46.png',
        'main/assets/images/aido_avatar_doc-47.png',
        'main/assets/images/aido_avatar_doc-48.png',
        'main/assets/images/aido_avatar_doc-49.png',
        'main/assets/images/aido_avatar_doc-50.png',
        'main/assets/images/aido_avatar_doc-51.png',
        'main/assets/images/aido_avatar_doc-52.png'
      ];

    }
    var test = angular.copy($scope.caseImages);
    $scope.arrOfArr = [];

    for (var i = 0; i < $scope.caseImages.length; i++) {
      if ((i % 4 ) === 0) {
        $scope.arrOfArr.push(test.splice(0, 4));
      }
    }
  };

  $scope.selectImage = function (path) {
    $scope.extra.defaultAvatar = path;
    $scope.closeModal();
  };

  $scope.showImages = function () {
    $scope.showModal('main/templates/selectImageModal.html');
  };

  $scope.showModal = function (templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.submitForm = function (isValid) {
    if (isValid) {
      $scope.newUserInfo.email = $scope.registerForm.email;
      $scope.newUserInfo.name = $scope.registerForm.nombre;
      $scope.newUserInfo.lastname = $scope.registerForm.apellido;
      $scope.newUserInfo.password = $scope.registerForm.pass;
      $scope.newUserInfo.role = $scope.registerForm.role.id;

      $ionicLoading.show({
        template: 'Creando cuenta...',
        showBackdrop: false,
        maxWidth: 0,
        duration: $rootScope.LOADER_TIME
      }).then(function () {

        aidoAPIFactory.createUser($scope.newUserInfo, $rootScope.csrfToken).then(function (response) {
          var userId = response.data.user.id;
          var role = $scope.newUserInfo.role;

          var loginInfo = {};
          loginInfo.email = $scope.newUserInfo.email;
          loginInfo.password = $scope.newUserInfo.password;

          // if (AuthService.isAuthenticated()) {
          //   AuthService.logout();
          // }

          if (role === 2) {
            AuthService.login(loginInfo, $rootScope.csrfToken).then(function () {
              $state.go('bienvenida', { newUserId: userId, newRoleId: role, avatar: $scope.extra.defaultAvatar });
            }, function (error) {
              Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
            });

          } else {
            $state.go('datosMedicos', { userId: userId, avatar: $scope.extra.defaultAvatar });
          }

        }, function (error) {
          Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
        });

      });
    }
  };

  $rootScope.$on('click-triggered', function () {
    console.log('click');
  });

});
