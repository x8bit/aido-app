'use strict';
angular.module('main')
.controller('EditarCasoCtrl', function ($scope, $stateParams, $rootScope, $ionicHistory, aidoAPIFactory, $ionicLoading, $ionicModal, ionicDatePicker, $ionicPopup, CameraService, AttachmentsService, Utils) {
  $scope.caseId = $stateParams.caseId;

  $scope.caso = {};
  $scope.caseImages = [
    'main/assets/images/aido_icon_NuevoCasoL.png',
    'main/assets/images/aido_avatar_caso-53.png',
    'main/assets/images/aido_avatar_caso-54.png',
    'main/assets/images/aido_avatar_caso-55.png',
    'main/assets/images/aido_avatar_caso-56.png',
    'main/assets/images/aido_avatar_caso-57.png',
    'main/assets/images/aido_avatar_caso-58.png',
    'main/assets/images/aido_avatar_caso-59.png',
    'main/assets/images/aido_avatar_caso-60.png',
    'main/assets/images/aido_avatar_caso-61.png',
    'main/assets/images/aido_avatar_caso-62.png',
    'main/assets/images/aido_avatar_caso-63.png',
    'main/assets/images/aido_avatar_caso-64.png',
    'main/assets/images/aido_avatar_caso-65.png',
    'main/assets/images/aido_avatar_caso-66.png'
  ];
  $scope.imagesData = [];
  $scope.imagesToUpload = [];

  function guid () {
    function s4 () {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  $scope.arrOfArrImages = [];


  var test = angular.copy($scope.caseImages);
  $scope.arrOfArr = [];

  for (var i = 0; i < $scope.caseImages.length; i++) {
    if ((i % 4 ) === 0) {
      $scope.arrOfArr.push(test.splice(0, 4));
    }
  }

  $scope.openDatePicker = function () {
    ionicDatePicker.openDatePicker({
      callback: function (val) {
        $scope.caso.symptomsDate = new Date(val);
        $scope.formatedDate = window.moment(new Date(val)).format('DD-MM-YYYY');
      }
    });
  };

  $scope.showModal = function (templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.closeModal = function () {
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.showImages = function () {
    $scope.showModal('main/templates/selectImageModal.html');
  };


  $scope.selectImage = function (path) {
    $scope.caso.defaultAvatar = path;
    $scope.closeModal();
  };


  $scope.showImagesPreview = function (index) {
    $scope.activeSlide = index;
    $scope.showModal('main/templates/image-popover.html');
  };

  $scope.attachment = {};
  $scope.symptomsList = [];

  aidoAPIFactory.getSymptoms().then(function (response) {
    $scope.symptomsList = response.data;
  });

  $scope.symptomsSelected = [];

  $scope.symptomsChange = function () {
    $scope.symptomsSelected = [];
    for (var i = 0; i < $scope.caso.symptoms.length; i++) {
      $scope.symptomsSelected.push($scope.caso.symptoms[i]);
    }
  };

  $scope.showAttachmentPopup = function () {
    $ionicPopup.show({
      template: '',
      cssClass: 'parentPop',
      title: 'Que deseas hacer?',
      scope: $scope,
      buttons: [
        {
          type: 'icon ion-ios-camera-outline',
          onTap: function () {
            $scope.takePhoto();
          }
        },
        {
          type: 'icon ion-paperclip',
          onTap: function () {
            $scope.choosePhoto();
          }
        },
        {
          type: 'icon ion-close',
        }
      ]
    });
  };


  $scope.takePhoto = function () {
    CameraService.takePhoto().then(function (base64String) {
      $scope.imagesData.push(base64String);
    });
  };

  $scope.eliminatePhoto = function (index) {
    $scope.imagesData.splice(index, 1);
    $scope.imagesToUpload.splice(index, 1);
  };

  $scope.choosePhoto = function () {
    CameraService.choosePhoto().then(function (base64String) {
      $scope.imagesData.push(base64String);
    });
  };

  $ionicLoading.show({
    template: 'Cargando caso...',
    showBackdrop: false,
    maxWidth: 0,
    duration: $rootScope.LOADER_TIME
  }).then(function () {
    aidoAPIFactory.getCaseDetail($scope.caseId).then(function (response) {
      $scope.caso = response;
      $scope.symptomsChange();
      $scope.formatedDate = window.moment($scope.caso.symptomsDate).format('DD-MM-YYYY');
      var test = angular.copy($scope.caso.firebaseAttachments);
      $scope.arrOfArrImages = [];

      for (var i = 0; i < $scope.caso.firebaseAttachments.length; i++) {
        if ((i % 4 ) === 0) {
          $scope.arrOfArrImages.push(test.splice(0, 4));
        }
      }
    });
  });

  $scope.save = function () {
    $ionicLoading.show({
      template: 'Guardando...',
      showBackdrop: false,
      maxWidth: 0,
      duration: $rootScope.LOADER_TIME
    }).then(function () {
      delete $scope.caso.chats;
      aidoAPIFactory.updateCase($scope.caseId, $scope.caso).then(function () {
        var attachmentsArray = [];

        if ($scope.caso.firebaseAttachments.length > 0) {
          attachmentsArray = $scope.caso.firebaseAttachments.slice();
        }

        CameraService.imagesToUpload.forEach(function (im) {
          var path = 'case_files/' + $scope.caso.id + '/' + guid();
          attachmentsArray.push(path);
          AttachmentsService.uploadBase64(path, im);
        });

        aidoAPIFactory.updateCaseAttachments($scope.caso.id, attachmentsArray).then(function () {
          CameraService.imagesToUpload = [];
        });
        Utils.alert('Se guardaron los cambios con exito', null, 'Aido', 'Aceptar');
      }, function (error) {
        Utils.alert(error.data.message, null, 'Aido', 'Aceptar');
      });
    });
  };
});
