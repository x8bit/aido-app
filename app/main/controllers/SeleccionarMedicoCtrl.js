'use strict';
angular.module('main')
.controller('SeleccionarMedicoCtrl', function ($scope, $stateParams, aidoAPIFactory, $state, $ionicLoading, $rootScope) {
  $scope.caseId = $stateParams.caseId;

  $scope.medicos = [];
  $scope.especialidades = [];
  $scope.searchMode = 'name';
  $scope.model = {};
  $scope.model.searchTerm = '';

  $scope.dictionary = {
    'name': 'Por nombre',
    'specialty': 'Por especialidad'
  };

  var clearLoaders = function () {
    $ionicLoading.hide();
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.doRefresh = function () {
    $scope.findMedics($scope.searchMode);
  };

  $scope.search = function () {
    $scope.findMedics($scope.searchMode);
    // if ($scope.model.searchTerm.length > 3) {
    //   $scope.findMedics($scope.searchMode);
    // } else if ($scope.model.searchTerm === '') {
    //   $scope.findMedics($scope.searchMode);
    // }
  };

  $scope.findMedics = function (searchMode) {
    $scope.searchMode = searchMode;

    $ionicLoading.show({
      template: 'Cargando..',
      showBackdrop: false,
      maxWidth: 0,
      duration: $rootScope.LOADER_TIME
    }).then(function () {

      if ($scope.searchMode === 'name') {
        aidoAPIFactory.getMedics($scope.model.searchTerm).then(function (response) {
          clearLoaders();
          $scope.medicos = response.data;
        }, function () {
          clearLoaders();
        });
      } else {
        aidoAPIFactory.getSpecialties($scope.model.searchTerm).then(function (response) {
          clearLoaders();
          $scope.especialidades = response.data;
        }, function () {
          clearLoaders();
        });
      }
    });
  };

  $scope.goToMedic = function (id) {
    $state.go('main.detailMedicoConsulta', { case: $scope.caseId, medic: id });
  };

  $scope.findMedics('name');

});
