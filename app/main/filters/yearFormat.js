'use strict';
angular.module('main')
.filter('yearFormat', function () {

  return function (input) {
    return window.moment().diff(input, 'years');
  };

});
