'use strict';
angular.module('main')
.filter('dateFormat', function () {

  return function (input) {
    return window.moment(input).locale('es').startOf('minute').fromNow();
  };

});
