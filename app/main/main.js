
'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'ngStorage',
  'firebase'
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider, $urlRouterProvider) {

  // ROUTING with ui.router
  $urlRouterProvider.otherwise('/main/listaDeCasos');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main', {
      url: '/main',
      cache: false,
      abstract: true,
      templateUrl: 'main/templates/menu.html',
      controller: 'MenuCtrl',
      data: {
        requiresLogin: true
      }
    })

    .state('login', {
      url: '/login',
      templateUrl: 'main/templates/login.html',
      controller: 'LoginCtrl',
      data: {
        requiresLogin: false
      }
    })

    .state('registro', {
      url: '/signup',
      templateUrl: 'main/templates/registro.html',
      controller: 'RegistroCtrl',
      data: {
        requiresLogin: false
      }
    })

    .state('recover', {
      url: '/recover',
      templateUrl: 'main/templates/recover.html',
      controller: 'RecoverCtrl',
      data: {
        requiresLogin: false
      }
    })

    .state('bienvenida', {
      url: '/bienvenida?newUserId,newRoleId,avatar',
      templateUrl: 'main/templates/bienvenida.html',
      controller: 'BienvenidaCtrl',
      data: {
        requiresLogin: false
      }
    })

    .state('bienvenidaMedico', {
      url: '/bienvenidaMedico',
      templateUrl: 'main/templates/bienvenidaMedico.html',
      controller: 'BienvenidaMedicoCtrl',
      params: {
        newUserId: null,
        newRoleId: null
      },
      data: {
        requiresLogin: false
      }
    })

    .state('datosPersonales', {
      url: '/datosPersonales',
      templateUrl: 'main/templates/datosPersonales.html',
      controller: 'DatosPersonalesCtrl',
      params: {
        userId: 23,
        roleId: 2,
        avatar: ''
      },
      data: {
        requiresLogin: false
      }
    })

    .state('datosMedicos', {
      url: '/datosMedicos?userId,avatar',
      templateUrl: 'main/templates/datosMedicos.html',
      controller: 'DatosMedicosCtrl',
      data: {
        requiresLogin: false
      }
    })

    .state('perfilMedico', {
      url: '/perfilMedico?userId,profileJSON',
      templateUrl: 'main/templates/perfil.html',
      controller: 'PerfilMedicoCtrl',
      data: {
        requiresLogin: false
      }
    })

    .state('main.crearCaso', {
      url: '/crearCaso',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/crearCaso.html',
          controller: 'CrearCasoCtrl'
        }
      },
      cache: false
    })

    .state('main.listaDeCasos', {
      url: '/listaDeCasos',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/listaDeCasos.html',
          controller: 'ListaDeCasosCtrl'
        }
      },
      cache: false
    })

    .state('main.editProfile', {
      url: '/editProfile',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/editProfile.html',
          controller: 'ProfileCtrl'
        }
      }
    })

    .state('main.seleccionarMedico', {
      url: '/seleccionarMedico?caseId',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/seleccionarMedico.html',
          controller: 'SeleccionarMedicoCtrl'
        }
      },
      cache: false
    })
    .state('main.detailMedicoConsulta', {
      url: '/detailMedicoConsulta?case&medic',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/detailMedicoConsulta.html',
          controller: 'DetailMedicoConsultaCtrl'
        }
      },
      cache: false
    })
    .state('main.payments', {
      url: '/payments',
      params: {
        userId: null,
        medic: null,
        case: null
      },
      views: {
        'pageContent': {
          templateUrl: 'main/templates/payments.html',
          controller: 'PaymentsCtrl'
        }
      },
      cache: false
    })
    .state('main.conekta', {
      url: '/conekta',
      params: {
        userId: null,
        medic: null,
        case: null
      },
      views: {
        'pageContent': {
          templateUrl: 'main/templates/conekta.html',
          controller: 'ConektaCtrl'
        }
      },
      cache: false
    })
    .state('main.casoDetail', {
      url: '/casoDetail/:id',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/caseDetail.html',
          controller: 'CaseDetailCtrl'
        }
      },
      cache: false
    })
    .state('main.chat', {
      url: '/chat/:id',
      cache: false,
      params: {
        caseId: null,
        id: null
      },
      views: {
        'pageContent': {
          templateUrl: 'main/templates/chatConsulta.html',
          controller: 'ChatCtrl'
        }
      }
    })

    .state('main.casoHistory', {
      url: '/history/:caseId',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/history.html',
          controller: 'HistoryCtrl'
        }
      },
      cache: false
    })

    .state('main.consultarReceta', {
      url: '/receta/:medicId/:caseTitle/:chatId',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/consultarReceta.html',
          controller: 'ConsultarRecetaCtrl'
        }
      },
      cache: false
    })

    .state('main.chatOptions', {
      url: '/options',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/chatOptions.html'
        }
      }
    })

    .state('main.invite', {
      url: '/invite',
      views: {
        'pageContent': {
          controller: 'InviteCtrl',
          templateUrl: 'main/templates/invite.html'
        }
      }
    })

    .state('main.chat.receta', {
      url: '/receta?chat&otherUser',
      views: {
        'pageContent@main': {
          controller: 'RecetaCtrl',
          templateUrl: 'main/templates/receta.html'
        }
      }
    })

    .state('main.terminos', {
      url: '/terminos',
      views: {
        'pageContent': {
          controller: 'TerminosCtrl',
          templateUrl: 'main/templates/terminos.html'
        }
      }
    })
    .state('main.contacto', {
      url: '/contacto',
      views: {
        'pageContent': {
          controller: 'ContactoCtrl',
          templateUrl: 'main/templates/contacto.html'
        }
      }
    })

    .state('main.editarCaso', {
      url: '/editarCaso/:caseId',
      views: {
        'pageContent': {
          templateUrl: 'main/templates/editarCaso.html',
          controller: 'EditarCasoCtrl'
        }
      },
      cache: false
    });
});
