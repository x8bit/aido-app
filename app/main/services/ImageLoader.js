'use strict';
angular.module('main')
.factory('ImageLoader', function () {

  var ImageLoader = {};

  ImageLoader.imagesToLoad = [];

  ImageLoader.setImage = function (url) {
    this.imagesToLoad.push(url);
  };

  ImageLoader.loaded = function (url) {
    this.imagesToLoad.splice(this.imagesToLoad.indexOf(url), 1);
  };

  return ImageLoader;

});
