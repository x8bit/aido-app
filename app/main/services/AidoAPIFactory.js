'use strict';
angular.module('main')
.factory('aidoAPIFactory', function ($q, $http, $rootScope, AuthService) {
  var aidoAPIFactory = {};

  function create (model, newDataJSON, csrfToken) {
    console.log(csrfToken);
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + model + '/',
      headers: {
        'Content-Type': 'application/json'
      },
      data: newDataJSON
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  }

  function get (model) {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + model + '/'}).then(function (data) {
      defer.resolve(data);
    }, function (error) {
      defer.reject(error);
    });

    return defer.promise;
  }

  aidoAPIFactory.createAttachment = function (newDataJSON, csrfToken) {
    return create('attachment', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createCase = function (newDataJSON, csrfToken) {
    return create('case', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createChatRoom = function (newDataJSON, csrfToken) {
    return create('chatroom', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createProfile = function (newDataJSON) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'profile',
      headers: {
        'Content-Type': 'application/json'
      },
      data: newDataJSON
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.createUser = function (newDataJSON, csrfToken) {
    return create('user', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createSymptom = function (newDataJSON, csrfToken) {
    return create('symptom', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createMessage = function (newDataJSON, csrfToken) {
    return create('message', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createNote = function (newDataJSON, csrfToken) {
    return create('note', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createProfile = function (newDataJSON, csrfToken) {
    return create('profile', newDataJSON, csrfToken);
  };

  aidoAPIFactory.createCaseHistory = function (newDataJSON, csrfToken) {
    return create('caseHistory', newDataJSON, csrfToken);
  };

  aidoAPIFactory.addToUser = function (userId, newDataJSON, csrfToken) {
    var URLstring = 'user/' + userId.toString();
    return create(URLstring, newDataJSON, csrfToken);
  };

  aidoAPIFactory.addToProfile = function (profileId, newDataJSON, csrfToken) {
    var URLstring = 'profile/' + profileId.toString();
    return create(URLstring, newDataJSON, csrfToken);
  };

  aidoAPIFactory.loginUser = function (newDataJSON, csrfToken) {
    return create('user/login', newDataJSON, csrfToken);
  };

  aidoAPIFactory.getCSRF = function () {
    return get('csrfToken');
  };

  aidoAPIFactory.updateOnlineStatus = function (userId, status) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'user/' + userId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        onlineStatus: status
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.updateCase = function (caseId, caseJSON) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'case/' + caseId + '?history=yes',
      headers: {
        'Content-Type': 'application/json'
      },
      data: caseJSON
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.updateCaseAttachments = function (caseId, attachmentsArray) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'case/' + caseId + '?history=no',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        firebaseAttachments: attachmentsArray
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.createPrescription = function (prescription) {
    var defer = $q.defer();
    var currentUser = AuthService.currentUserId();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'prescription?user=' + currentUser,
      headers: {
        'Content-Type': 'application/json'
      },
      data: prescription
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.addPrescriptionToChat = function (chatId, prescriptionId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'chatRoom/' + chatId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        prescription: [prescriptionId]
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.closeCase = function (caseId) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'case/closeCase/' + caseId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        status: 'closed'
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.openCase = function (caseId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'case/' + caseId + '?history=yes',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        status: 'active'
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.closeChat = function (chatId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'chatRoom/' + chatId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'status': 'closed'
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.openChat = function (chatId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'chatRoom/' + chatId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'status': 'open'
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.getPrescription = function (medicId, chatId) {
    var url = 'prescription/findByMedic';
    var defer = $q.defer();
    $http({
      method: 'POST',
      url: $rootScope.api_url + url,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'id': medicId,
        'chat': chatId
      }
    })
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getRoles = function () {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'role/getAppRoles'})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getSymptoms = function () {
    return get('symptom');
  };

  aidoAPIFactory.getUser = function (userId) {
    var URLstring = 'user/' + userId.toString();
    return get(URLstring);
  };

  aidoAPIFactory.getNotes = function (chatRoomId) {
    var URLstring = 'note?chatNote=' + chatRoomId.toString();
    return get(URLstring);
  };

  aidoAPIFactory.getUserbyEmail = function (userEmail) {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'user/findPatient?email=' + userEmail})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getProfile = function (profileId) {
    var URLstring = 'profile/' + profileId;
    return get(URLstring);
  };

  aidoAPIFactory.getMedics = function (search) {
    var url = (search === '') ? 'user/findMedics' : 'user/findMedics?search=' + search;
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + url})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getCasesForMedic = function (medic) {
    var url = 'case/findMedicCases?user=' + medic;
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + url})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getChatRoom = function (chatId) {
    var URLstring = 'chatroom/' + chatId.toString();
    return get(URLstring);
  };

  aidoAPIFactory.getCases = function (user, status) {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'case?patient=' + user + '&status=' + status})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getCaseDetail = function (caseId) {
    var defer = $q.defer();
    $http.get($rootScope.api_url + 'case/' + caseId).then(function (response) {
      defer.resolve(response.data);
    }, function (error) {
      defer.reject(error);
    });
    return defer.promise;
  };

  aidoAPIFactory.getMedicCases = function (user, status) {
    console.log(status);
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'case/findMedicCases?user=' + user + '&status=' + status})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getSpecialties = function (search) {
    var url = (search === undefined || search === null) ? 'specialty/' : 'specialty/findMedics?search=' + search;
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + url})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getHistory = function (caseId) {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'caseHistory/findByCase?caseId=' + caseId})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.getMedicines = function () {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'medicine' })
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.createMedicine = function (medicine) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'medicine',
      headers: {
        'Content-Type': 'application/json'
      },
      data: { name: medicine }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.createSymptom = function (symptom) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'symptom',
      headers: {
        'Content-Type': 'application/json'
      },
      data: { name: symptom }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.searchByEmail = function (email) {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'user/findPatient?email=' + email})
      .then(function (response) {
        defer.resolve(response.data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.sendInvitation = function (email, fromEmail) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'user/inviteUser',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {'email': email, 'fromEmail': fromEmail}
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.sendNewInvitation = function (email, fromEmail) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'user/inviteNewUser',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {'email': email, 'fromEmail': fromEmail}
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.recoverPassword = function (email) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'user/forgot',
      headers: {
        'Content-Type': 'application/json'
      },
      data: { 'email': email }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.sendReport = function (reportJSON) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'user/sendReport',
      headers: {
        'Content-Type': 'application/json'
      },
      data: reportJSON
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.paypal = function () {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/addPaypal',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        patient: 13,
        medic: 68,
        case: 0
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.getCustomer = function (id) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/getCustomer',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.createCustomer = function (id, token) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/createCustomer',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id,
        token: token
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.createPaymentSource = function (id, token) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/createPaymentSource',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id,
        token: token
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.updatePaymentSource = function (id, cardInfo, index) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/updatePaymentSource',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id,
        cardInfo: cardInfo,
        index: index
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.deletePaymentSource = function (id, index) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/deletePaymentSource',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id,
        index: index
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.createOrder = function (id, customer, medic, payment) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/createOrder',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id,
        customerId: customer,
        medicId: medic,
        paymentSource: payment
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.createTransaction = function (id, medic, payment) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'transaction/createTransaction',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        userId: id,
        medicId: medic,
        paymentId: payment
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.sendNotification = function (to, title, msg, data) {
    var defer = $q.defer();
    var req = {
      method: 'POST',
      url: $rootScope.api_url + 'notification/send',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        to: to,
        title: title,
        message: msg,
        payload: data,
        multiple: true
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });
    return defer.promise;
  };

  aidoAPIFactory.getConfig = function () {
    var defer = $q.defer();
    $http({method: 'GET', url: $rootScope.api_url + 'config'})
      .then(function (data) {
        defer.resolve(data);
      }, function (error) {
        defer.reject(error);
      });
    return defer.promise;
  };

  aidoAPIFactory.unregisterToken = function (userId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'user/' + userId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'device_token': ''
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.setActiveChat = function (userId, chatId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'user/' + userId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'activeChat': chatId
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.clearActiveChat = function (userId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'user/' + userId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'activeChat': null
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.updateTransaction = function (transactionId, chatId) {
    var defer = $q.defer();
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'transaction/' + transactionId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        'consultation': chatId
      }
    };
    $http(req).then(function (data) {
      return defer.resolve(data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.clearCaseNotifications = function (caseId) {
    var defer = $q.defer();
    var data = {};
    if (AuthService.isCurrentUserPatient()) {
      data = {
        'patientBadge': 0
      };
    } else {
      data = {
        'medicBadge': 0
      };
    }
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'case/' + caseId + '?history=yes',
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.updateCaseNotifications = function (caseId, count) {
    var defer = $q.defer();
    var data = {};
    if (AuthService.isCurrentUserPatient()) {
      data = {
        'patientBadge': count
      };
    } else {
      data = {
        'medicBadge': count
      };
    }
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'case/' + caseId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  aidoAPIFactory.clearChatNotifications = function (chatId) {
    var defer = $q.defer();
    var data = {};
    if (AuthService.isCurrentUserPatient()) {
      data = {
        'patientMessageCount': 0
      };
    } else {
      data = {
        'medicMessageCount': 0
      };
    }
    var req = {
      method: 'PUT',
      url: $rootScope.api_url + 'chatRoom/' + chatId,
      headers: {
        'Content-Type': 'application/json'
      },
      data: data
    };
    $http(req).then(function (data) {
      return defer.resolve(data.data);
    }, function (status) {
      return defer.reject(status);
    });

    return defer.promise;
  };

  return aidoAPIFactory;
});
