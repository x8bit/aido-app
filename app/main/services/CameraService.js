'use strict';
angular.module('main')
.service('CameraService', function ($cordovaCamera, $q) {

  var CAMERA_OPTIONS = {
    quality: 75,
    destinationType: window.Camera.DestinationType.DATA_URL,
    sourceType: window.Camera.PictureSourceType.CAMERA,
    allowEdit: false,
    encodingType: window.Camera.EncodingType.JPEG,
    targetWidth: 800,
    targetHeight: 800,
    popoverOptions: window.CameraPopoverOptions,
    saveToPhotoAlbum: false
  };
  var GALLERY_OPTIONS = {
    quality: 75,
    destinationType: window.Camera.DestinationType.DATA_URL,
    sourceType: window.Camera.PictureSourceType.PHOTOLIBRARY,
    allowEdit: false,
    encodingType: window.Camera.EncodingType.JPEG,
    targetWidth: 800,
    targetHeight: 800,
    popoverOptions: window.CameraPopoverOptions,
    saveToPhotoAlbum: false
  };

  // var CAMERA_OPTIONS = {};
  // var GALLERY_OPTIONS = {};

  this.imagesToUpload = [];

  this.takePhoto = function () {
    var ref = this;
    var defer = $q.defer();

    $cordovaCamera.getPicture(CAMERA_OPTIONS).then(function (imageData) {
      ref.imagesToUpload.push(imageData);
      defer.resolve('data:image/jpeg;base64,' + imageData);
    }, function (err) {
      defer.reject(err);
    });

    return defer.promise;
  };

  this.choosePhoto = function () {
    var ref = this;
    var defer = $q.defer();

    $cordovaCamera.getPicture(GALLERY_OPTIONS).then(function (imageData) {
      ref.imagesToUpload.push(imageData);
      defer.resolve('data:image/jpeg;base64,' + imageData);
    }, function (err) {
      defer.reject(err);
    });

    return defer.promise;
  };

});
