'use strict';
angular.module('main')
.factory('NotificationService', function ($q, $localStorage, aidoAPIFactory) {

  var NotificationService = {};

  var init = function () {
    if (!$localStorage.notifications) {
      $localStorage.notifications = {};
    }
    if (!$localStorage.chatNotifications) {
      $localStorage.chatNotifications = {};
    }
  };

  NotificationService.clean = init;

  NotificationService.updateNotification = function (caseId) {
    var count = $localStorage.notifications[caseId] || 0;
    $localStorage.notifications[caseId] = ++count;
  };

  NotificationService.clearNotification = function (caseId) {
    delete $localStorage.notifications[caseId];
    aidoAPIFactory.clearCaseNotifications(caseId);
  };

  NotificationService.getNotification = function (caseId) {
    init();
    return $localStorage.notifications[caseId] || 0;
  };

  NotificationService.setNotification = function (caseId, counter) {
    init();
    return $localStorage.notifications[caseId] = counter;
  };

  NotificationService.updateChatNotification = function (chatId) {
    var count = $localStorage.chatNotifications[chatId] || 0;
    $localStorage.chatNotifications[chatId] = ++count;
  };

  NotificationService.clearChatNotification = function (chatId) {
    delete $localStorage.chatNotifications[chatId];
    aidoAPIFactory.clearChatNotifications(chatId);
  };

  NotificationService.getChatNotification = function (chatId) {
    init();
    return $localStorage.chatNotifications[chatId] || 0;
  };

  NotificationService.setChatNotification = function (chatId, counter) {
    init();
    return $localStorage.chatNotifications[chatId] = counter;
  };

  return NotificationService;

});
