'use strict';
angular.module('main')
.factory('AttachmentsService', function ($q) {

  var AttachmentsService = {};
  var Storage = window.firebase.storage();
  var Reference = Storage.ref();

  AttachmentsService.uploadBase64 = function (ref, data) {
    var tempRef = Reference.child(ref);
    var defer = $q.defer();
    tempRef.putString(data, 'base64', { contentType: 'image/jpeg' }).then(function (snapshot) {
      defer.resolve(snapshot);
    });
    return defer.promise;
  };

  return AttachmentsService;

});
