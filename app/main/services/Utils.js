'use strict';
angular.module('main')
.factory('Utils', function ($ionicPopup) {

  var utils = {};

  utils.alert = function (msg, opt, title, btn) {
    console.log(opt, btn);
    $ionicPopup.alert({
      title: title,
      template: msg
    });
  };

  return utils;

});
