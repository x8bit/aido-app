'use strict';
angular.module('main')
.factory('AuthInterceptor', function ($q, $injector, $localStorage) {

  return {

    request: function (config) {
      var token;
      if ($localStorage.token) {
        token = $localStorage.token;
      }
      if (token) {
        config.headers.Authorization = 'Bearer ' + token;
      }
      return config;
    },

    responseError: function (response) {
      if (response.status === 401 || response.status === 403) {

        if ($localStorage.session.id) {
          $injector.get('AuthService').refreshToken($localStorage.session.id).then(function () {
          });
          //return $http(response.config);

        } else {
          delete $localStorage.token;
          delete $localStorage.session;
          delete $localStorage.notifications;
          delete $localStorage.chatNotifications;
          $injector.get('$state').go('login');
        }

      }
      return $q.reject(response);
    }

  };

});
