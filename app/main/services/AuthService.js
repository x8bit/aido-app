'use strict';
angular.module('main')
.factory('AuthService', function ($q, $http, $localStorage, $rootScope) {

  var authService = {};

  authService.promise = null;

  authService.isAuthenticated = function () {
    return ($localStorage.token) ? true : false;
  };

  authService.clearSession = function () {
    delete $localStorage.token;
    delete $localStorage.session;
    delete $localStorage.notifications;
    delete $localStorage.chatNotifications;
  };

  authService.register = function (newuser) {
    var self = this;
    self.clearSession();

    return $http.post($rootScope.api_url + 'user', newuser).success(function (response) {
      $localStorage.token = response.token;
      $localStorage.session = response.user;
    });
  };

  authService.sendTokenToServer = function () {
    return $http.post($rootScope.api_url + 'user/receiveToken', { 'device_token': $localStorage.notifToken, user: $localStorage.session.id, platform: window.ionic.Platform.device().platform });
  };

  authService.deleteTokenFromServer = function () {
    return $http.post($rootScope.api_url + 'user/deleteToken', { 'device_token': $localStorage.notifToken, user: $localStorage.session.id, platform: window.ionic.Platform.device().platform });
  };

  authService.refreshToken = function (userId) {
    return $http.post($rootScope.api_url + 'user/refreshToken', { user: userId }).success(function (data) {
      $localStorage.token = data.token;
      $localStorage.session = data.user;
    });
  };

  authService.logout = function () {
    var self = this;
    self.deleteTokenFromServer();
    self.clearSession();
  };

  authService.login = function (credentials) {
    var self = this;

    return $http({
      method: 'POST',
      url: $rootScope.api_url + 'user/login',
      headers: {
        'Content-Type': 'application/json'
      },
      data: credentials
    }).success(function (data) {
      $localStorage.token = data.token;
      $localStorage.session = data.user;

      if (typeof $localStorage.notifToken !== 'undefined') {
        self.sendTokenToServer();
      }
      return data;
    });
  };

  authService.getSession = function () {
    var self = this;
    return this.promise = $http({ method: 'GET', url: $rootScope.api_url + 'user/sess', cache: false })
      .success(function () {
        return self.sessionInfo();
      })
      .error(function () {
        self.clearSession();
      });
  };

  authService.sessionInfo = function () {
    return $localStorage;
  };

  authService.currentUserId = function () {
    return $localStorage.session.id;
  };

  authService.isCurrentUserPatient = function () {
    return $localStorage.session.role[0].id === 2;
  };

  // como solo se usa para agregar el perfil a una cuenta que no lo tenia inicialmente, no se debe reemplazar completamente la sessión
  // para dudas revisar en donde se esta mandando llamar la función
  authService.refreshSession = function (newSessionJSON) {
    $localStorage.session.profile = {};
    $localStorage.session.profile = newSessionJSON;
  };

  authService.refreshOnlineStatus = function (status) {
    $localStorage.session.onlineStatus = status;
  };

  return authService;

});
