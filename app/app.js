'use strict';
angular.module('Aido', [
  // load your modules here
  'main', // starting with the main module
  'ionic-modal-select',
  'ionic-datepicker',
  'ngIOS9UIWebViewPatch',
  'creditCardInput'

]).config(function ($ionicConfigProvider, $httpProvider, ionicDatePickerProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');

  var datePickerObj = {
    inputDate: new Date(),
    setLabel: 'Ok',
    todayLabel: 'Hoy',
    closeLabel: 'Cerrar',
    mondayFirst: false,
    weeksList: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    monthsList: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    templateType: 'popup',
    showTodayButton: true,
    dateFormat: 'dd MMM yyyy',
    closeOnSelect: false
  };
  ionicDatePickerProvider.configDatePicker(datePickerObj);

}).run(function ($ionicPlatform, $rootScope, AuthService, $state,  NotificationService, $localStorage, aidoAPIFactory, Utils, $ionicHistory) {
  $rootScope.csrfToken = '';
  // $rootScope.api_url = 'http://35.165.97.7/api/';
  $rootScope.api_url = 'http://52.11.251.122/api/';
  //$rootScope.api_url = 'http://127.0.0.1:1337/';
  $rootScope.LOADER_TIME = 500;

  // var checkSession = function () {
  //   return AuthService.getSession().then(function () {
  //     return AuthService.isAuthenticated();
  //   });
  // };

  $rootScope.$on('$stateChangeStart', function (event, toState) {
    var stateData = toState.data;
    if (!AuthService.isAuthenticated() && stateData.requiresLogin) {
      event.preventDefault();
      return $state.go('login');
    }
  });

  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    var chatId = 0;
    if (toState.name === 'main.chat') {
      chatId = parseInt(toParams.id);
      aidoAPIFactory.setActiveChat($localStorage.session.id, chatId);
    } else if (fromState.name === 'main.chat') {
      chatId = parseInt(fromParams.id);
      aidoAPIFactory.clearActiveChat($localStorage.session.id);
    }
  });

  $rootScope.$on('tokenRegistered', function () {
    if (AuthService.isAuthenticated()) {
      AuthService.sendTokenToServer();
    }
  });

  $ionicPlatform.ready(function () {

    $ionicPlatform.on('pause', function () {
      if ($state.current.name === 'main.chat') {
        aidoAPIFactory.clearActiveChat($localStorage.session.id);
      }
    });

    $ionicPlatform.on('resume', function () {
      if ($state.current.name === 'main.chat') {
        var chatId = parseInt($state.params.id);
        aidoAPIFactory.setActiveChat($localStorage.session.id, chatId);
      }
    });

    if (window.Connection) {
      var internetConnected = true;
      $rootScope.$on('$cordovaNetwork:offline', function () {
        if (!internetConnected) {
          return;
        }
        internetConnected = false;
        Utils.alert('Es necesario contar con conexión a internet para utilizar Aido', null, 'Aido', 'Aceptar');
      });

      $rootScope.$on('$cordovaNetwork:online', function () {
        if (internetConnected) {
          return;
        }
        internetConnected = true;
      });
    }

    if (window.PushNotification) {
      window.push = window.PushNotification.init({
        android: {
          forceShow: true
        },
        ios: {
          alert: 'true',
          badge: 'true',
          sound: 'true'
        }
      });

      window.PushNotification.hasPermission(function (data) {
        if (data.isEnabled) {
          console.log('isEnabled');
        }
      });

      window.push.on('registration', function (data) {
        $localStorage.notifToken = data.registrationId;
        $rootScope.$broadcast('tokenRegistered', { token: $localStorage.notifToken });
      });

      window.push.on('notification', function (data) {
        var payload = data.additionalData;

        switch (payload.tipo) {
          case 'mensaje':
            NotificationService.updateNotification(payload.caseId);
            NotificationService.updateChatNotification(payload.chatId);
            var count = parseInt(NotificationService.getChatNotification(payload.chatId));
            var old = parseInt(NotificationService.getNotification(payload.caseId));

            $ionicHistory.nextViewOptions({
              disableBack: true
            });

            if (data.additionalData.foreground !== true) {
              NotificationService.setNotification(payload.caseId, (old - count));
              NotificationService.clearChatNotification(payload.chatId);
              $state.go('main.chat', { caseId: payload.caseId, id: payload.chatId });
            } else if ($state.current.name === 'main.chat') {
              if (parseInt($state.params.caseId) === parseInt(payload.caseId) && parseInt($state.params.id) === parseInt(payload.chatId)) {
                NotificationService.setNotification(payload.caseId, (old - count));
                NotificationService.clearChatNotification(payload.chatId);
              }
            }
            break;
          case 'caso':
            var roleIsPatient = (AuthService.sessionInfo().session.role[0].id === 2) ? true : false;
            if (data.additionalData.foreground !== true) {
              if (roleIsPatient) {
                if ($state.current.name !== 'main.casoDetail') {
                  $state.go('main.casoDetail', { id: payload.caseId });
                }
              } else {
                if ($state.current.name !== 'main.chat') {
                  $state.go('main.chat', { caseId: payload.caseId, id: payload.chatId });
                }
              }
            }
            break;
          case 'nota':
            if (data.additionalData.foreground !== true) {
              $state.go('main.casoHistory', { caseId: payload.caseId });
            }
            break;
          case 'receta':
            if (data.additionalData.foreground !== true) {
              $state.go('main.consultarReceta', { medicId: payload.medic, caseTitle: payload.title, chatId: payload.chatId});
            }
            break;
        }

        window.push.finish(function () {
          console.log('processing of push data is finished');
        }, function () {
          console.log('something went wrong with push.finish for ID = ' + data.additionalData.notId);
        }, data.additionalData.notId);

        $rootScope.$broadcast('onPushNotification', payload);
      });
    }

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      window.cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      window.StatusBar.styleDefault();
      window.StatusBar.backgroundColorByHexString('#1E447B');
      // window.StatusBar.overlaysWebView(false);
    }
  });
});
